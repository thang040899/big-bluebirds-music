﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace BigBluebirdsMusic.Models
{
    public class Song
    {
        // Su dung de luu vao localDB, thong tin bai hat cua playlist
        [PrimaryKey, AutoIncrement]
        public int LocalID { get; set; }
        public int LocalPlayListID { get; set; }

        // ID online
        public int ID { get; set; }
        public string Name { get; set; }
        public string ImageLink { get; set; }
        public string Singer { get; set; }
        public string Type { get; set; }
        public string DateCreate { get; set; }
        public int TotalListen { get; set; }
        public int TotalLike { get; set; }
        public string LinkLyric { get; set; }
        public int Duration { get; set; }
        public string DurationString { get; set; }
        public string LinkMp3 { get; set; }
        public bool IsLocal { get; set; }

        public Song()
        {
            IsLocal = true;
            ID = 0;
            Name = "";
            ImageLink = "m_music.png";
            Type = null;
            Singer = "";
            Duration = 0;
            LinkMp3 = "acb";
            //LinkLyric = @"https://static-zmp3.zadn.vn/lyrics/e/6/a/6/e6a6be98fb0201dd9f99db41d6ceee9a.lrc";
            DurationString = GetDurationString(Duration);
        }
        public Song(Song song)
        {
            this.ID = song.ID;
            this.Name = song.Name;
            this.ImageLink = song.ImageLink;
            this.Singer = song.Singer;
            this.Type = song.Type;
            this.DateCreate = song.DateCreate;
            this.TotalListen = song.TotalListen;
            this.TotalLike = song.TotalLike;
            this.LinkLyric = song.LinkLyric;
            this.Duration = song.Duration;
            this.DurationString = song.DurationString;
            this.LinkMp3 = song.LinkMp3;
            this.IsLocal = song.IsLocal;
        }

        public static string GetDurationString(int Duration)
        {
            string m = Duration / 60 + "";
            string s = Duration % 60 + "";
            if (m.Length < 2)
                m = "0" + m;
            if (s.Length < 2)
                s = "0" + s;
            return m + ":" + s;
        }

        public string GenerateJson()
        {
            return "{ID:"+ this.ID 
                + ", Name:\""+this.Name
                +"\", ImageLink:\""+this.ImageLink
                +"\", Singer:\""+this.Singer
                +"\", DurationString:\""+this.DurationString
                +"\", Duration:"+this.Duration
                +", Link:\""+this.LinkMp3
                +"\", IsLocal:\""+this.IsLocal+"\"}";
        }
        public static string TotalIntToString(int total)
        {
            if (total > 1000000) 
            {
                return total / 1000000 + "M";
            }
            else if (total > 1000)
            {
                return total / 1000 + "K";
            }
            else
            {
                return total + "";
            }
        }
    }

}

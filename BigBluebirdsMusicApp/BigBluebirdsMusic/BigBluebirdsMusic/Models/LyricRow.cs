﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BigBluebirdsMusic.Models
{
    public class LyricRow
    {
        public int Time { get; set; } //Second
        public string Content { get; set; }
    }
}

﻿using BigBluebirdsMusic.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace BigBluebirdsMusic.Models
{
    public class User
    {
        private static User _instance=null;
        public static User GetInstance()
        {
            if (_instance == null)
            {
                _instance = new User();
                GetLocalInstanceInfo();
            }
            else if (!_instance.IsLoggedIn) 
            {
                GetLocalInstanceInfo();
            }
            return _instance;
        }
        public static void GetLocalInstanceInfo()
        {
            User user = _instance;
            if (user == null)
                user = new User();
            if (user.IsLoggedIn == false)
            {
                User localInfo = FileRW.ReadUserInfor();
                if (localInfo == null)
                {
                    return;
                }
                if (localInfo.IsLoggedIn == false)
                {
                    //Chua dang nhap that
                    return;
                }
                else
                {
                    user = localInfo;
                }
            }
            _instance = user;
            //Neu login roi thi thoi
        }
        public static void SetInstance(User user)
        {
            _instance = user;
            _instance.SaveInfoToFile();
        }
        public String ID { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public DateTime DayOfBirdth { get; set; }
        public String PhoneNumber { get; set; }
        public String UserName { get; set; }
        public String Email { get; set; }
        public String Role { get; set; }
        public String Thumbnail { get; set; }
        public String Token { get; set; }
        public bool IsLoggedIn { get; set; }

        public String FullName => $"{FirstName} {LastName}";
        public User()
        {
            IsLoggedIn = false;
        }

        public string GenerateJson()
        {
            return "{UserID: \"" + this.ID
                + "\", FirstName:\"" + this.FirstName
                + "\", LastName:\"" + this.LastName
                + "\", DayOfBirdth:\"" + this.DayOfBirdth.ToString("yyyy/MM/ddT00:00:00")
                + "\", PhoneNumber:\"" + this.PhoneNumber
                + "\", UserName:\"" + this.UserName
                + "\", Email:\"" + this.Email
                + "\", Role:\"" + this.Role
                + "\", IsLoggedIn:\"" + this.IsLoggedIn
                + "\", Thumbnail:\"" + this.Thumbnail + "\"}";
        }
        public void SaveInfoToFile()
        {
            FileRW.SaveUserInfor(this);
        }
        public static User LogoutLocalUser()
        {
            _instance = new User();
            FileRW.SaveUserInfor(_instance);
            return _instance;
        }
    }
}

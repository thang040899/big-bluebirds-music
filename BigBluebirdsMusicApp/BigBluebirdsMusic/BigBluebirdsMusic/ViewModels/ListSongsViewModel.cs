﻿using BigBluebirdsMusic.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace BigBluebirdsMusic.ViewModels
{
    public class ListSongsViewModel
    {
        public ObservableCollection<Song> songs { get; set; }
        public ICommand AddSongCommand => new Command(AddSong);
        public ICommand RemoveSongCommand => new Command(RemoveSong);
        public void AddSong(object obj)
        {
            Song song = (Song)obj;
            songs.Add(song);
        }
        public void RemoveSong(object obj)
        {
            Song song = (Song)obj;
            songs.Remove(song);
        }

        public ListSongsViewModel()
        {
            songs = new ObservableCollection<Song>(new Playlist().Songs);
        }
        public ListSongsViewModel(List<Song> lstsongs)
        {
            songs = new ObservableCollection<Song>(lstsongs);
        }
        
        
    }
}

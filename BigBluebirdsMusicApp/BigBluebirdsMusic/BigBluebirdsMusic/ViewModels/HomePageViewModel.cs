﻿using BigBluebirdsMusic.Models;
using BigBluebirdsMusic.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace BigBluebirdsMusic.ViewModels
{
    public class HomePageViewModel: System.ComponentModel.INotifyPropertyChanged
    {
        private static HomePageViewModel instance = null;

        public event PropertyChangedEventHandler PropertyChanged;

        public static HomePageViewModel GetInstance()
        {
            if (instance == null)
            {
                instance = new HomePageViewModel();
            }
            return instance;
        }

        private ObservableCollection<Song> recentSongs { get; set; }
        public ObservableCollection<Song> RecentSongs
        {
            get { return recentSongs; }
            set
            {

                recentSongs = value;
            }
        }
        private ObservableCollection<Song> recommendSongs { get; set; }
        public ObservableCollection<Song> RecommendSongs
        {
            get { return recommendSongs; }
            set { recommendSongs = value; }
        }
        private ObservableCollection<Song> topSongs { get; set; }
        public ObservableCollection<Song> TopSongs
        {
            get { return topSongs; }
            set { topSongs = value; }
        }

        private ObservableCollection<Playlist> recommendPlaylists { get; set; }
        public ObservableCollection<Playlist> RecommendPlaylists
        {
            get { return recommendPlaylists; }
            set { recommendPlaylists = value; }
        }

        private int heightLVRecentSong { get; set; }
        private int heightSTRecentSong { get; set; }
        private int heightLVRecommendSong { get; set; }
        private int heightSTRecommendSong { get; set; }

        public int HeightLVRecentSong 
        {
            get { return heightLVRecentSong; }
            set
            {
                heightLVRecentSong = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs("HeightLVRecentSong"));
                }
            }
        }
        public int HeightSTRecentSong 
        {
            get { return heightSTRecentSong; }
            set
            {
                heightSTRecentSong = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs("HeightSTRecentSong"));
                }
            }
        }
        public int HeightLVRecommendSong
        {
            get { return heightLVRecommendSong; }
            set
            {
                heightLVRecommendSong = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs("HeightLVRecommendSong"));
                }
            }
        }
        public int HeightSTRecommendSong
        {
            get { return heightSTRecommendSong; }
            set
            {
                heightSTRecommendSong = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs("HeightSTRecommendSong"));
                }
            }
        }
        public ICommand GetRecentSongsCommand => new Command(GetRecentSongs);
        public ICommand GetRecommendSongsCommand => new Command(GetRecommendSongs);
        public ICommand GetTopSongsCommand => new Command(GetTopSongs);
        public ICommand GetRecommendPlaylistsCommand => new Command(GetRecommendPlaylists);

        public void GetRecentSongs(object obj)
        {
            // Remove old
            int count = recentSongs.Count;
            for (int i=0; i < count; i++)
                RecentSongs.Remove(recentSongs[0]);
            // Add new
            foreach(Song song in FileRW.ReadListRecentSongs())
            {
                RecentSongs.Add(song);
            }
            HeightLVRecentSong = 70 * recentSongs.Count + 20;
            HeightSTRecentSong = HeightLVRecentSong + 60;
        }

        public async void GetRecommendSongs(object obj)
        {
            try
            {

                List<Song> lstRecommendUserBase = await DataService.GetListSongsRecommendUserBase();
                int count = recommendSongs.Count;
                for (int i = 0; i < count; i++)
                {
                    recommendSongs.Remove(recommendSongs[0]);
                }

                foreach(var item in lstRecommendUserBase)
                {
                    recommendSongs.Add(item);
                }
                HeightLVRecommendSong = 60 * recommendSongs.Count + 10;
                HeightSTRecommendSong = 70 + HeightLVRecommendSong;
                List<Song> lstRecommendItemBase = new List<Song>();
                List<Song> lstRecommendFinal = new List<Song>();

                var lstRecent = FileRW.ReadListRecentSongs();
                //lstRecent.Reverse();
                foreach (var item in lstRecent)
                {
                    if (item.IsLocal)
                        continue;
                    foreach(var item2 in await DataService.GetListSongsRecommendItemBase(item.ID))
                    {
                        lstRecommendItemBase.Add(item2);
                    }
                }
                int recommendLength = 5;
                int recommendItemLength = 3;
                int recommendUserLength = recommendLength - recommendItemLength;
                if (lstRecommendItemBase.Count == 0)
                {
                    lstRecommendFinal = lstRecommendUserBase;
                }
                else if (!User.GetInstance().IsLoggedIn)
                {
                    lstRecommendFinal = lstRecommendItemBase;
                }
                else
                {
                    foreach(var item2 in lstRecommendItemBase.GetRange(0, recommendItemLength))
                    {
                        lstRecommendFinal.Add(item2);
                    }
                    foreach (var item2 in lstRecommendUserBase.GetRange(0, recommendUserLength))
                    {
                        lstRecommendFinal.Add(item2);
                    }
                }

                count = recommendSongs.Count;
                for (int i = 0; i < count; i++)
                {
                    recommendSongs.Remove(recommendSongs[0]);
                }
                foreach (var item in lstRecommendFinal)
                {
                    if (recommendSongs.Count >= 5)
                        break;
                    bool isExist = false;
                    foreach(var item2 in recommendSongs)
                    {
                        if (item.ID == item2.ID)
                        {
                            isExist = true;
                            break;
                        }
                    }
                    if(!isExist)
                        recommendSongs.Add(item);
                }
                    

                HeightLVRecommendSong = 60 * recommendSongs.Count + 10;
                HeightSTRecommendSong = 70 + HeightLVRecommendSong;
            }
            catch { }
        }

        public async void GetRecommendPlaylists(object obj)
        {
            try
            {
                var lstRecommendPlaylist = await DataService.GetRecommendedPlaylists();
                int count = recommendPlaylists.Count;
                for (int i = 0; i < count; i++)
                {
                    recommendPlaylists.Remove(recommendPlaylists[0]);
                }

                foreach (var item in lstRecommendPlaylist)
                {
                    recommendPlaylists.Add(item);
                }
            }
            catch { }
            
        }

        public async void GetTopSongs(object obj)
        {
            try
            {
                List<Song> lstSongs = await DataService.GetTopSongs();
                if (lstSongs.Count>0)
                {
                    int count = topSongs.Count;
                    for (int i = 0; i < count; i++)
                        topSongs.Remove(topSongs[0]);
                    foreach (var item in lstSongs.GetRange(0, 5))
                        topSongs.Add(item);
                }
            }
            catch { }
        }

        public HomePageViewModel()
        {
            recentSongs = new ObservableCollection<Song>(FileRW.ReadListRecentSongs());
            HeightLVRecentSong = 70 * recentSongs.Count + 20;
            HeightSTRecentSong = HeightLVRecentSong + 60;

            topSongs = new ObservableCollection<Song>();
            recommendSongs = new ObservableCollection<Song>();
            recommendPlaylists = new ObservableCollection<Playlist>();
        }
        
    }
}

﻿using BigBluebirdsMusic.Models;
using BigBluebirdsMusic.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace BigBluebirdsMusic.ViewModels
{
    public class ProfileViewModel : System.ComponentModel.INotifyPropertyChanged
    {
        private static ProfileViewModel instance;
        public static ProfileViewModel GetInstance()
        {
            if (instance == null)
                instance = new ProfileViewModel();
            return instance;
        }


        #region FRIEND
        private ObservableCollection<BigBluebirdsMusic.Models.User> friends { get; set; }
        public ObservableCollection<BigBluebirdsMusic.Models.User> Friends
        {
            get { return friends; }
            set
            {
                friends = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs("Friends"));
                }
            }
        }
        public ICommand AddFriendCommand => new Command(AddFriend);
        public ICommand RemoveFriendCommand => new Command(RemoveFriend);
        public ICommand ClearFriendCommand => new Command(ClearFriend);
        public ICommand SetFriendCommand => new Command(SetFriend);

        public event PropertyChangedEventHandler PropertyChanged;

        public void AddFriend(object obj)
        {
            User friend = (User)obj;
            Friends.Add(friend);
        }
        public void RemoveFriend(object obj)
        {
            User friend = (User)obj;
            Friends.Remove(friend);
        }
        public void ClearFriend(object obj)
        {
            Friends.Clear();
        }
        public void SetFriend(object obj)
        {
            List<User> lstFriend = (List<User>)obj;
            Friends.Clear();
            foreach (User item in lstFriend)
            {
                Friends.Add(item);
            }
        }

        public async void LoadFriendAsync(ListView lv_friend)
        {
            lv_friend.IsRefreshing = true;
            List<User> friends1 = await DataService.GetAllFolowFriendAsync();
            List<User> friends = new List<User>();
            var friends_id = friends1.Select(x => x.ID).Distinct().ToList();
            foreach (var user in friends1)
            {
                if (friends_id.Contains(user.ID))
                {
                    friends.Add(user);
                    friends_id.Remove(user.ID);
                }
            }
            SetFriendCommand.Execute(friends);
            lv_friend.HeightRequest = 6 * 60 + 20;
            lv_friend.IsRefreshing = false;
        }

        #endregion


        public ProfileViewModel()
        {
            friends = new ObservableCollection<User>(new List<User>());
            BarInforVisible = true;
            BarLoginVisible = false;
        }
        public ProfileViewModel(List<User> lstfriends)
        {
            friends = new ObservableCollection<User>(lstfriends);
        }
        
        #region AVATAR
        public string avatar { get; set; }
        public ICommand SetAvatarCommand => new Command(SetAvatar);
        public void SetAvatar(object url)
        {
            avatar = (string)url;
        }
        #endregion

        #region user info
        private User userInfo { get; set; }
        public User UserInfo
        {
            get { return userInfo; }
            set
            {
                userInfo = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs("UserInfo"));
                }
            }
        }

        public ICommand SetUserInfoCommand => new Command(SetUserInfo);
        public ICommand OfflineUserInfoCommand => new Command(OfflineUserInfo);
        public void SetUserInfo(object obj)
        {
            UserInfo = (User)obj;
            BarInforVisible = true;
            BarLoginVisible = false;
            LoadFriendAsync(new ListView());
        }
        public void OfflineUserInfo(object obj)
        {
            UserInfo = new User {
                ID = "0",
                FirstName = "Người dùng 1",
                LastName = "",
                Role = "VIP: 0",
                IsLoggedIn = false,
                Thumbnail = "",
            };
            BarInforVisible = false;
            BarLoginVisible = true;
        }
        #endregion

        private bool barInforVisible { get; set; }
        public bool BarInforVisible
        {
            get { return barInforVisible; }
            set
            {
                barInforVisible = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs("BarInforVisible"));
                }
            }
        }

        private bool barLoginVisible { get; set; }
        public bool BarLoginVisible
        {
            get { return barLoginVisible; }
            set
            {
                barLoginVisible = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs("BarLoginVisible"));
                }
            }
        }


    }
}

﻿using BigBluebirdsMusic.Models;
using BigBluebirdsMusic.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace BigBluebirdsMusic.ViewModels
{
    public class UserOnlineSongsViewModel: System.ComponentModel.INotifyPropertyChanged
    {
        private static UserOnlineSongsViewModel instance = null;

        public event PropertyChangedEventHandler PropertyChanged;

        public static UserOnlineSongsViewModel GetInstance()
        {
            if (instance == null)
            {
                instance = new UserOnlineSongsViewModel();
            }
            return instance;
        }

        public ObservableCollection<Song> songs { get; set; }
        public UserOnlineSongsViewModel()
        {
            songs = new ObservableCollection<Song>();
            IsAddingOrEditing = true;
        }
        public ICommand AddSongCommand => new Command(AddSong);
        public ICommand RemoveSongCommand => new Command(RemoveSong);
        public ICommand GetSongsOfCurrentUserCommand => new Command(GetSongsOfCurrentUser);
        public void AddSong(object obj)
        {
            Song song = (Song)obj;
            songs.Add(song);
        }
        public void RemoveSong(object obj)
        {
            Song song = (Song)obj;
            songs.Remove(song);
        }
    
        public async void GetSongsOfCurrentUser(object obj)
        {
            int count = songs.Count;
            for(int i=0; i < count; i++)
            {
                songs.Remove(songs[0]);
            }
            foreach(var item in await DataService.GetOnlineSongsOfUser())
            {
                if(obj==null)
                    songs.Add(item);
                else if (item.Name.ToLower().Contains(((string)obj).ToLower()))
                {
                    songs.Add(item);
                }
                
            }
        }

        private bool isAddingOrEditing { get; set; }
        //True = adding; false = edingting
        public bool IsAddingOrEditing
        {
            get { return isAddingOrEditing; }
            set
            {
                isAddingOrEditing = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs("IsAddingOrEditing"));
                }
            }
        }
    }
}

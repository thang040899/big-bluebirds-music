﻿using Android.Media;
using BigBluebirdsMusic.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BigBluebirdsMusic.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LocalPage : ContentPage
    {
        public LocalPage()
        {
            InitializeComponent();
        }

        private async void ExcuteFrameFadedAnimation(object sender, EventArgs e)
        {
            const int _animationTime = 50;
            try
            {
                var layout = (Frame)sender;
                await layout.FadeTo(0.5, _animationTime);
                await layout.FadeTo(1, _animationTime);
            }
            catch { }
        }
        private void btn_song_offline_Tapped(object sender, EventArgs e)
        {
            ExcuteFrameFadedAnimation(sender, e);
            Navigation.PushAsync(new LocalMusicPage(), false);
        }
        private void btn_playlist_offline_Tapped(object sender, EventArgs e)
        {
            ExcuteFrameFadedAnimation(sender, e);
            Navigation.PushAsync(new LocalPlaylistPage(), true);
        }

        private async void btn_song_online_Tapped(object sender, EventArgs e)
        {
            if (!User.GetInstance().IsLoggedIn)
            {
                //Show hoi dang nhap
                bool answer = await DisplayAlert("Chưa đăng nhập", "Bạn có muốn đăng nhập?", "Vâng", "Lúc khác");
                if (answer)
                {
                    //Ensure Logout beforelogin
                    User.LogoutLocalUser();
                    await Navigation.PushAsync(new LoginPage());
                }
                return;
            }
            //logged in
            await Navigation.PushAsync(new MySongsOnlinePage());
        }

        private async void btn_playlist_online_Tapped(object sender, EventArgs e)
        {
            if (!User.GetInstance().IsLoggedIn)
            {
                //Show hoi dang nhap
                bool answer = await DisplayAlert("Chưa đăng nhập", "Bạn có muốn đăng nhập?", "Vâng", "Lúc khác");
                if (answer)
                {
                    //Ensure Logout beforelogin
                    User.LogoutLocalUser();
                    await Navigation.PushModalAsync(new LoginPage());
                }
                return;
            }
            await Navigation.PushAsync(new LocalPlaylistPage(isOnlinePage:true));
        }
    }
}
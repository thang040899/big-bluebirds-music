﻿using BigBluebirdsMusic.Models;
using BigBluebirdsMusic.Services;
using MediaManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BigBluebirdsMusic.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlaylistInfoPage : ContentPage
    {
        public Playlist Playlist { get; set; }
        public PlaylistInfoPage(Playlist playlist)
        {
            InitializeComponent();
            this.Playlist = playlist;
            
            imgPlaylist.Source = playlist.ImageLink;
            lbPlaylistName.Text = playlist.Name;
            lbPlaylistNumSong.Text = playlist.NumberSong.ToString() + " bài hát";
            lbPlaylistDescription.Text = playlist.Description;
            lb_totalListen.Text = Song.TotalIntToString(playlist.TotalListen);
            lb_totalLike.Text = Song.TotalIntToString(playlist.TotalLike);
            GetSong();
        }

        private async void GetSong()
        {
            lvListSong.IsRefreshing = true;
            if (!Playlist.IsLocal)
            {
                Playlist tmp = await DataService.GetPlaylistInforAsync(Playlist.ID.ToString());
                if (tmp == null)
                    Playlist.Songs = new List<Song>();
                else if (tmp.Songs == null)
                    Playlist.Songs = new List<Song>();
                else
                    Playlist.Songs = tmp.Songs;
            }
            lvListSong.ItemsSource = Playlist.Songs;
            lvListSong.IsRefreshing = false;
        }
        private async void lvListSong_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            PlayingPage.GetInstance().PlaySong(Playlist.Songs[e.ItemIndex]);
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            
        }

        [Obsolete]
        private async void MenuItem_Download_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            string message = "";
            if (song.IsLocal)
            {
                message = "Bài hát đã được tải xuống.";
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
                return;
            }

            try
            {
                message = "Đang tải xuống bài hát " + song.Name;
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
                await new Downloader().SaveFilePublicStorageAsync(song.Name + "-" + song.ID + ".mp3", song.LinkMp3);
            }
            catch
            {
                message = "Tải xuống bài hát thất bại!";
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
            }
            try
            {
                await new Downloader().SaveFilePublicStorageAsync(song.Name + "-" + song.ID + ".lyric", song.LinkLyric);
            }
            catch { }

            message = "Đã tải xuống bài hát " + song.Name;
            Android.Widget.Toast.MakeText(
                Android.App.Application.Context,
                message,
                Android.Widget.ToastLength.Short).Show();
        }

        private void MenuItem_AddPlaylist_Clicked(object sender, EventArgs e)
        {
            pll_view.IsEnabled = false;
            pll_view.Opacity = 0.3;
            context_menu_add.IsVisible = true;
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            this.addingSong = song;
        }
        private Song addingSong = null;
        private bool isLiked = false;
        private async void btn_like_Pressed(object sender, EventArgs e)
        {
            if (!User.GetInstance().IsLoggedIn)
            {
                //Show hoi dang nhap
                bool answer = await DisplayAlert("Chưa đăng nhập", "Bạn có muốn đăng nhập?", "Vâng", "Lúc khác");
                if (answer)
                {
                    //Ensure Logout beforelogin
                    User.LogoutLocalUser();
                    await Navigation.PushModalAsync(new LoginPage());
                }
                return;
            }
            if (Playlist.IsLocal)
            {
                // Toast bai hat offline
                string message = "Danh sách phát offline!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
                return;
            }
            if (isLiked)
            {
                isLiked = false;
                btn_like.Source = "ni_heart_off.png";
            }
            else
            {
                isLiked = true;
                btn_like.Source = "ni_heart_fill.png";
            }
            //Call api like dislike
            Playlist new_info = await DataService.LikeOrDisLikeAsync(isLiked, Playlist.ID);
            if (new_info != null)
            {
                //Set label like, listen
                lb_totalLike.Text = Song.TotalIntToString(Int32.Parse(new_info.TotalLike.ToString()));
                lb_totalListen.Text = Song.TotalIntToString(Int32.Parse(new_info.TotalListen.ToString()));
            }
        }

        private void btn_playAll_Clicked(object sender, EventArgs e)
        {
            PlayingPage.GetInstance().PlayPlaylist(Playlist, 0);
        }
        private void MenuItem_AddPlayingList_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            try
            {
                MediaManager.Library.MediaItem mediaItem = new MediaManager.Library.MediaItem(song.LinkMp3);
                CrossMediaManager.Current.Queue.Add(mediaItem);
                CrossMediaManager.Current.Queue.MediaItems.Add(mediaItem);
                PlayingPage.GetInstance().vmPlayinglist.AddSong(song);
                string message = "Đã thêm bài hát vào danh sách đang phát!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
            }
            catch
            {
                string message = "Thêm bài hát vào danh sách đang phát thất bại!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
            }

        }

        private void btn_add_to_offline_playlist_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new LocalPlaylistPage(true, addingSong), true);
            btn_cancel_Clicked(sender, e);
        }

        private async void btn_add_to_online_playlist_Clicked(object sender, EventArgs e)
        {
            if (addingSong.IsLocal)
            {
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    "Không thể thêm bài hát trên máy vào danh sách online.",
                    Android.Widget.ToastLength.Short).Show();
                return;
            }
            if (!User.GetInstance().IsLoggedIn)
            {
                bool answer = await DisplayAlert("Chưa đăng nhập", "Bạn có muốn đăng nhập?", "Vâng", "Lúc khác");
                if (answer)
                {
                    //Ensure Logout beforelogin
                    User.LogoutLocalUser();
                    await Navigation.PushModalAsync(new LoginPage());
                }
                return;
            }
            await Navigation.PushAsync(new LocalPlaylistPage(true, addingSong, true), true);
            btn_cancel_Clicked(sender, e);
        }

        private void btn_cancel_Clicked(object sender, EventArgs e)
        {
            pll_view.IsEnabled = true;
            pll_view.Opacity = 1;
            context_menu_add.IsVisible = false;
        }
    }
}
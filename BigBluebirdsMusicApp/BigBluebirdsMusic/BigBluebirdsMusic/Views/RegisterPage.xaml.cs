﻿using Android;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BigBluebirdsMusic.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
            btn_show_pass.Source = "";
        }
        private bool isUsernameValid = false;
        private bool isPasswordValid = false;
        private void txt_username_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidUsernameInput();
        }

        private async void btn_register_Clicked(object sender, EventArgs e)
        {
            if(isPasswordValid && isUsernameValid)
                await Navigation.PushModalAsync(new RegisterInforPage(txt_username.Text, txt_pass.Text));
        }

        private void TapGestureRecognizer_Tapped_1(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }
        private void ValidUsernameInput()
        {
            Regex regex = new Regex(@"^([A-Za-z][A-Za-z0-9._]{6,30})$");
            Match match = regex.Match(txt_username.Text);
            string error_mesage = "";
            if (!match.Success)
            {
                error_mesage = "Tên đăng nhập chứa ký tự không hợp lệ!";
            }
            if (txt_username.Text.Length < 6)
                error_mesage = "Tên đăng nhập tối thiểu 6 ký tự";
            if (error_mesage != "")
            {
                lb_username_failed.Text = error_mesage;
                lb_username_failed.IsVisible = true;
                isUsernameValid = false;
            }
            else
            {
                lb_username_failed.IsVisible = false;
                isUsernameValid = true;
            }
                
        }

        private void ValidatePasswordInput()
        {
            Regex regex = new Regex(@"^(?=.*([A-Z]){1,})(?=.*[!@#$&*]{1,})(?=.*[0-9]{1,})(?=.*[a-z]{1,}).{1,100}$");
            Match match = regex.Match(txt_pass.Text);
            string error_mesage = "";
            if (txt_pass.Text.Length < 8)
                error_mesage = "Mật khẩu tối thiểu 8 ký tự";
            if (!match.Success)
            {
                error_mesage = "Mật khẩu gồm chữ hoa, chữ thường, số và ký tự đặc biệt";
            }

            if (error_mesage != "")
            {
                lb_failed.Text = error_mesage;
                lb_failed.IsVisible = true;
                isPasswordValid = false;
            }
            else
            {
                lb_failed.IsVisible = false;
                isPasswordValid = true;
            }
                
        }

        private void txt_pass_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidatePasswordInput();
        }

        private void btn_showPass_Tapped(object sender, EventArgs e)
        {
            txt_pass.IsPassword = !txt_pass.IsPassword;
            try
            {
                txt_pass.CursorPosition = txt_pass.Text.Length;
            }
            catch { }
            
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using MediaManager;
using MediaManager.Library;
using MediaManager.Media;
using MediaManager.Playback;
using MediaManager.Player;
using MediaManager.Queue;
using Xamarin.Forms;
using PositionChangedEventArgs = MediaManager.Playback.PositionChangedEventArgs;
using Xamarin.Forms.Xaml;
using BigBluebirdsMusic.Models;
using System.Diagnostics;
using BigBluebirdsMusic.ViewModels;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using Plugin.Share;
using Plugin.Share.Abstractions;
using BigBluebirdsMusic.Services;

namespace BigBluebirdsMusic.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlayingPage : ContentPage
    {
        private static PlayingPage instance=null;
        public static PlayingPage GetInstance()
        {
            if (instance == null)
            {
                instance = new PlayingPage();
            }
            return instance;
        }
        public PlayingPage()
        {
            if (instance != null)
                throw new Exception("Playing page is singleton");
            InitializeComponent();
            BindingContext = this;
            btnPlay.Source = "icon_play.png";
            CrossMediaManager.Current.Init();
            CrossMediaManager.Current.StateChanged += Current_OnStateChanged;
            CrossMediaManager.Current.PositionChanged += Current_PositionChanged;
            CrossMediaManager.Current.MediaItemChanged += Current_MediaItemChanged;

            InitPlayingList();
        }

        public void InitPlayingList()
        {
            vmPlayinglist = new ListSongsViewModel();
            vmComments = new CommentsViewModel();
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            lb_Name.Text = SongName;
            lb_Singer.Text = SingerName;
            lvPlayingList.BindingContext = vmPlayinglist;
            lvPlayingList.SetBinding(ListView.ItemsSourceProperty, "songs");
            lst_comment.BindingContext = vmComments;
            lst_comment.SetBinding(ListView.ItemsSourceProperty, "comments");
            // lvPlayingList.SetBinding(ListView.SelectedItemProperty, "SelectedItem");
            var animation = new Animation(v => img_music.Rotation = v, 0, 360);
            animation.Commit(this, "SimpleAnimation", 16, 2000, Easing.Linear, (v, c) => img_music.Rotation = 0, () => true);
            
            if (!CrossMediaManager.Current.IsPrepared())
            {
                //await InitPlay();

                //// Set up Player Preferences
                //CrossMediaManager.Current.ShuffleMode = ShuffleMode.All;
                //CrossMediaManager.Current.PlayNextOnFailed = true;
                //CrossMediaManager.Current.RepeatMode = RepeatMode.All;
                //CrossMediaManager.Current.AutoPlay = true;
            }
            else
            {
                //SetupCurrentMediaDetails(CrossMediaManager.Current.Queue.Current);
                SetupCurrentMediaPositionData(CrossMediaManager.Current.Position);
                SetupCurrentMediaPlayerState(CrossMediaManager.Current.State);
            }

        }

        private void SetupCurrentMediaPositionData(TimeSpan position)
        {
            var formattingPattern = @"hh\:mm\:ss";
            if (CrossMediaManager.Current.Duration.Hours <= 0)
                formattingPattern = @"mm\:ss";

            var fullLengthString = CrossMediaManager.Current.Duration.ToString(formattingPattern);
            lbTime.Text = $"{position.ToString(formattingPattern)}/{fullLengthString}";

            progressBar.Value = position.Ticks;
            SetLyric((int)(position.Ticks / 10000000));
        }
        private void SetLyric(int time)
        {
            try
            {
                LyricRow row = lyrics.Where(x => x.Time == time).First();
                if (row != null)
                {
                    lv_lyrics.SelectedItem = row;
                    lv_lyrics.ScrollTo(row, ScrollToPosition.MakeVisible, true);
                }
                    
            }
            catch { }
        }
        private async Task InitPlay()
        {
            //var songList = new List<string>() {
            //    "/storage/emulated/0/Zing MP3/Yêu Một Người Tổn Thương_Nhật Phong_-1079444563.mp3"
            //};
            //var currentMediaItem = await CrossMediaManager.Current.Play(songList);
            ////SetupCurrentMediaDetails(currentMediaItem);
            //Console.WriteLine(currentMediaItem.ToString());
        }

        private void SetControlStatus()
        {
            if (loopStatus ==LOOP_STATUS.LOOP_ALL)
            {
                CrossMediaManager.Current.RepeatMode = RepeatMode.All;
            }
            else if(loopStatus == LOOP_STATUS.LOOP_ONE)
            {
                CrossMediaManager.Current.RepeatMode = RepeatMode.One;
            }
            else
            {
                CrossMediaManager.Current.RepeatMode = RepeatMode.Off;
            }

            if (isShuffleOn)
            {
                CrossMediaManager.Current.ShuffleMode = ShuffleMode.All;
            }
            else
            {
                CrossMediaManager.Current.ShuffleMode = ShuffleMode.Off;
            }
            CrossMediaManager.Current.PlayNextOnFailed = true;
            CrossMediaManager.Current.AutoPlay = true;
        }
        public void PlaySong(Song song)
        {
            Models.Playlist pll = new Models.Playlist();
            pll.Songs = new List<Song>();
            pll.Songs.Add(song);
            // var currentMediaItem = await CrossMediaManager.Current.Play(song.LinkMp3);
            PlayPlaylist(pll, 0);
            //SetControlStatus();
            //SetScreenPlayInfo(song);
        }
        public async void PlayPlaylist(Models.Playlist playlist, int index)
        {
            try
            {
                vmPlayinglist.songs.Clear();
                vmPlayinglist.songs = new ObservableCollection<Song>(new List<Song>());
                //List<MediaItem> mediaItems = new List<MediaItem>();
                if (playlist.Songs.Count==1)
                {
                    Song song = playlist.Songs[0];
                    await CrossMediaManager.Current.Play(new MediaItem()
                    {
                        DisplayTitle = song.Name,
                        Author = song.Singer,
                        Artist = song.Singer,
                        DisplayImageUri = song.ImageLink,
                        MediaUri = song.LinkMp3,
                        DisplayDescription = "..."
                    });
                    vmPlayinglist.AddSong(song);
                }
                else
                {

                    for(int i=0; i < playlist.Songs.Count; i++)
                    {
                        //Them vao UI
                        Song song = playlist.Songs[i];
                        vmPlayinglist.AddSong(song);

                        if (i == index)
                        {
                            // Play
                            await CrossMediaManager.Current.Play(new MediaItem()
                            {
                                DisplayTitle = song.Name,
                                Author = song.Singer,
                                Artist = song.Singer,
                                DisplayImageUri = song.ImageLink,
                                MediaUri = song.LinkMp3,
                                DisplayDescription = "..."
                            });
                        }
                        else
                        {
                            // Them vao QUEUE
                            MediaItem mediaItem = new MediaItem()
                            {
                                DisplayTitle = song.Name,
                                Author = song.Singer,
                                Artist = song.Singer,
                                DisplayImageUri = song.ImageLink,
                                MediaUri = song.LinkMp3,
                                DisplayDescription = "..."
                            };
                            CrossMediaManager.Current.Queue.Add(mediaItem);
                        }
                    }
                    playingIndex = index;
                    await CrossMediaManager.Current.Play();
                }
                
                lvPlayingList.ItemsSource = vmPlayinglist.songs;
                //lvPlayingList.SelectedItem = vmPlayinglist.songs[index];
                lvPlayingList.SelectedItem = lvPlayingList.ItemsSource.Cast<Song>().ToList()[index];

            }
            catch {}
        }

        public void SetScreenPlayInfo(Song song)
        {
            try
            {
                SongName = "♪ " + (song.Name.Length >= 30 ? song.Name.Substring(0, 30) + "..." : song.Name) + " ♫";
                SingerName = "- " + song.Singer + " -";
                try
                {
                    CrossMediaManager.Current.Queue.Current.Title = song.Name;
                }
                catch { }
                try
                {
                    lb_Name.Text = SongName;
                    lb_Singer.Text = SingerName;
                    lb_totalListen.Text = Song.TotalIntToString(song.TotalListen);
                    lb_totalLike.Text = Song.TotalIntToString(song.TotalLike);
                }
                catch { }
                btnPlay.Source = "icon_pause.png";
            }
            catch 
            {
                Console.WriteLine("ATDEMO: SetScreenPlayInfo failed");
            }
        }


        #region HOOK_EVENT
        private void Current_MediaItemChanged(object sender, MediaItemEventArgs e)
        {
            //SetupCurrentMediaDetails(e.MediaItem);
            try
            {
                playingIndex = ((MediaQueue)sender).CurrentIndex;
                lvPlayingList.SelectedItem = lvPlayingList.ItemsSource.Cast<Song>().ToList()[playingIndex];
                
            }
            catch { }
        }

        private void Current_PositionChanged(object sender, MediaManager.Playback.PositionChangedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                SetupCurrentMediaPositionData(e.Position);
            });
        }

        private void Current_OnStateChanged(object sender, StateChangedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                SetupCurrentMediaPlayerState(e.State);
            });
        }

        private void SetupCurrentMediaPlayerState(MediaPlayerState currentPlayerState)
        {
            lbTime.Text = $"{currentPlayerState.ToString().ToUpper()}";

            if (currentPlayerState == MediaManager.Player.MediaPlayerState.Loading)
            {
                progressBar.Value = 0;
            }
            else if (currentPlayerState == MediaManager.Player.MediaPlayerState.Playing
                    && CrossMediaManager.Current.Duration.Ticks > 0)
            {
                progressBar.Maximum = CrossMediaManager.Current.Duration.Ticks;

            }
        }
        
        #endregion

        #region Prop
        public int playingIndex = 0;
        //public List<Song> playingList;
        public List<Comment> listComments;
        public ListSongsViewModel vmPlayinglist;
        public CommentsViewModel vmComments;
        public bool isShuffleOn = false;
        public LOOP_STATUS loopStatus = LOOP_STATUS.LOOP_ALL;
        public static string SongName { get; set; }
        public static string SingerName { get; set; }
        List<LyricRow> lyrics { get; set; }
        #endregion

        #region Event
        public PlayingPageStatus playingPageStatus = PlayingPageStatus.NON;
        private async void btnPlay_Clicked(object sender, EventArgs e)
        {
            if (!CrossMediaManager.Current.IsPrepared())
            {
                await InitPlay();
            }
            else
            {
                await CrossMediaManager.Current.PlayPause();
            }

            if (CrossMediaManager.Current.IsPlaying())
            {
                btnPlay.Source = "icon_play.png";
            }
            //Neu dang pause thi play
            else
            {
                btnPlay.Source = "icon_pause.png";
                await CrossMediaManager.Current.Play();
            }
        }
        private void btnPre_Clicked(object sender, EventArgs e)
        {
            if (vmPlayinglist.songs.Count <= 1)
            {
                return;
            }
            int pre = playingIndex;
            if (isShuffleOn)
            {
                pre = new Random().Next(0, vmPlayinglist.songs.Count - 1);
            }
            else
            {
                if (pre <=0)
                {
                    pre = vmPlayinglist.songs.Count -1;
                }
                else
                {
                    pre--;
                }
            }
            playingIndex = pre;
            lvPlayingList.SelectedItem = lvPlayingList.ItemsSource.Cast<Song>().ToList()[pre];
            //lvPlayingList.SelectedItem = vmPlayinglist.songs[pre];
        }

        private void btnShuffle_Clicked(object sender, EventArgs e)
        {
            isShuffleOn = !isShuffleOn;
            if (isShuffleOn)
            {
                btnShuffle.Source = "ni_shuffle_bl.png";
                try
                {
                    //CrossMediaManager.Current.ShuffleMode = ShuffleMode.All;
                }
                catch(Exception ex)
                { 
                
                }
                
            }
            else
            {
                btnShuffle.Source = "ni_shuffle_off.png";
                try
                {
                    //CrossMediaManager.Current.ShuffleMode = ShuffleMode.Off;
                }
                catch { }
            }

        }

        private async void btnNext_Clicked(object sender, EventArgs e)
        {
            if (vmPlayinglist.songs.Count <= 1)
            {
                return;
            }
            int next = playingIndex;
            if (isShuffleOn)
            {
                next = new Random().Next(0, vmPlayinglist.songs.Count - 1);
            }
            else
            {
                if (next >= vmPlayinglist.songs.Count - 1)
                {
                    next = 0;
                }
                else
                {
                    next++;
                }
            }
            playingIndex = next;

            //vmPlayinglist.SelectedItem = vmPlayinglist.songs[next];
            lvPlayingList.SelectedItem = lvPlayingList.ItemsSource.Cast<Song>().ToList()[next];

        }

        private void btnLoop_Clicked(object sender, EventArgs e)
        {
            if(loopStatus == LOOP_STATUS.LOOP_OFF)
            {
                loopStatus = LOOP_STATUS.LOOP_ALL;
                btnLoop.Source = "ni_loop_bl.png";
                try
                {
                    CrossMediaManager.Current.RepeatMode = RepeatMode.All;
                }
                catch { }
            }
            else if (loopStatus == LOOP_STATUS.LOOP_ALL)
            {
                loopStatus = LOOP_STATUS.LOOP_ONE;
                btnLoop.Source = "ni_loop_one_bl.png";
                try
                {
                    CrossMediaManager.Current.RepeatMode = RepeatMode.One;
                }
                catch { }
            }
            else if (loopStatus == LOOP_STATUS.LOOP_ONE)
            {
                loopStatus = LOOP_STATUS.LOOP_OFF;
                btnLoop.Source = "ni_loop_off.png";
                try
                {
                    CrossMediaManager.Current.RepeatMode = RepeatMode.Off;
                }
                catch { }
            }
        }

        private void btn_comment_Pressed(object sender, EventArgs e)
        {
            if (vmPlayinglist.songs.Count == 0)
                return;
            if (playingPageStatus == PlayingPageStatus.COMMENT)
            {
                playingPageStatus = PlayingPageStatus.NON;
                reset();
            }
            else
            {
                playingPageStatus = PlayingPageStatus.COMMENT;
                reset();
                comment_frame.IsVisible = true;
                btn_comment.Source = "ni_cmt.png";
                LoadComment();
                
            }
        }
        private async void LoadComment()
        {
            Song song = (Song)vmPlayinglist.songs[playingIndex];
            if (song.IsLocal)
            {
                listComments = new List<Comment>();
                vmComments.SetComments(listComments);
                return;
            }
            lst_comment.IsRefreshing = true;
            listComments = await DataService.GetCommentsAsync(song.ID);
            vmComments.SetComments(listComments);
            //lst_comment.ItemsSource = listComments;
            lst_comment.IsRefreshing = false;
        }

        private void reset()
        {
            non_frame.IsVisible = true;
            comment_frame.IsVisible = false;
            lyric_frame.IsVisible = false;
            playlist_frame.IsVisible = false;
            //info_frame.IsVisible = false;
            btn_comment.Source = "ni_cmt_off.png";
            btn_lyric.Source = "ni_mic_off.png";
            btn_playingList.Source = "ni_playlist_off.png";
        }
        private void btn_lyric_Pressed(object sender, EventArgs e)
        {
            if (playingPageStatus == PlayingPageStatus.LYRIC)
            {
                playingPageStatus = PlayingPageStatus.NON;
                reset();
            }
            else
            {
                playingPageStatus = PlayingPageStatus.LYRIC;
                reset();
                lyric_frame.IsVisible = true;
                btn_lyric.Source = "ni_mic.png";
            }
        }

        private async void btn_share_Pressed(object sender, EventArgs e)
        {
            if (vmPlayinglist.songs.Count == 0)
                return;
            if (vmPlayinglist.songs[playingIndex].IsLocal)
            {
                string message = "Không thể chia sẻ bài hát offline!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
                return;
            }
            string link = "http://bigbluebirds.vn/songs/"+(vmPlayinglist.songs[playingIndex]).ID;
            var msg = new ShareMessage { Text = link };
            await CrossShare.Current.Share(msg);
        }

        private void btn_info_Pressed(object sender, EventArgs e)
        {
            if (playingPageStatus == PlayingPageStatus.INFO)
            {
                playingPageStatus = PlayingPageStatus.NON;
                reset();
            }
            else
            {
                playingPageStatus = PlayingPageStatus.INFO;
                reset();
                //info_frame.IsVisible = true;
            }
        }

        private async void btn_send_Clicked(object sender, EventArgs e)
        {
            string txt = tbx_comment.Text;
            Song song = (Song)vmPlayinglist.songs[playingIndex];
            if (!User.GetInstance().IsLoggedIn)
            {
                //Show hoi dang nhap
                bool answer = await DisplayAlert("Chưa đăng nhập", "Bạn có muốn đăng nhập?", "Vâng", "Lúc khác");
                if (answer)
                {
                    //Ensure Logout beforelogin
                    User.LogoutLocalUser();
                    await Navigation.PushModalAsync(new LoginPage());
                }
                return;
            }
            if (song.IsLocal)
            {
                // Toast bai hat offline
                string message = "Bài hát offline!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
                return;
            }
            bool res= await DataService.CommentAsync(song.ID, txt);
            if (!res)
            {
                string message = "Comment thất bại!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
            }
            else
            {
                User user = User.GetInstance();
                string avatar;
                if (user.Thumbnail == "" || user.Thumbnail == null)
                    avatar = "m_musicicon.png";
                else
                    avatar = user.Thumbnail;
                Comment cmt = new Comment()
                {
                    Content = txt,
                    Time = DateTime.Now.ToString("dd/MM/yyyy-HH:mm"),
                    UserName = user.FirstName + " " + user.LastName,
                    ImageLink = avatar,
                };
                listComments.Add(cmt);
                vmComments.AddComment(cmt);
                tbx_comment.Text = "";
                //lst_comment.ItemsSource = listComments;
            }
        }

        private void btn_playingList_Pressed(object sender, EventArgs e)
        {
            try
            {
                if (playingPageStatus == PlayingPageStatus.PLAYLIST)
                {
                    playingPageStatus = PlayingPageStatus.NON;
                    reset();
                }
                else
                {
                    playingPageStatus = PlayingPageStatus.PLAYLIST;
                    reset();
                    playlist_frame.IsVisible = true;
                    btn_playingList.Source = "ni_playlist.png";
                    //lvPlayingList.SelectedItem = vmPlayinglist.songs[playingIndex];
                    lvPlayingList.SelectedItem = lvPlayingList.ItemsSource.Cast<Song>().ToList()[playingIndex];
                    //vmPlayinglist.SelectedItem = vmPlayinglist.songs[playingIndex];
                }
            }
            catch { }
            
        }

        private async void lvPlayingList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                Song s = (Song)e.SelectedItem;
                if (!(s.IsLocal && s.Duration == 0))
                {
                    FileRW.SaveRecentSongs((Song)e.SelectedItem);
                    try
                    {
                        HomePageViewModel.GetInstance().GetRecentSongsCommand.Execute(null);
                    }
                    catch { }
                }

                lvPlayingList.ScrollTo(e.SelectedItem, ScrollToPosition.MakeVisible, false);
            }
            catch {
                Console.WriteLine("AT: Save recent song faild.");
            }
            
            try
            {
                Song song = (Song)e.SelectedItem;
                await CrossMediaManager.Current.Play();
                int index = lvPlayingList.ItemsSource.Cast<Song>().ToList().IndexOf((Song)e.SelectedItem);
                playingIndex = index;
                await CrossMediaManager.Current.PlayQueueItem(index);
                SetControlStatus();
                SetScreenPlayInfo(song);
                SetLyric(song.LinkLyric);
                GetLikeStatus(song);
                LoadComment();
                if(!song.IsLocal)
                    DataService.ListenSong(song.ID);
                await CrossMediaManager.Current.Play();
            }
            catch { }
        }
        private bool isLiked = false;
        private async void GetLikeStatus(Song song)
        {
            if (song.IsLocal)
            {
                btn_like.Source = "ni_heart_off.png";
                return;
            }
            // Goi api
            // Check user like chua
            isLiked = await DataService.CheckLikeSongAsync(song.ID);
            if (isLiked)
            {
                btn_like.Source = "ni_heart_fill.png";
            }
            else
            {
                btn_like.Source = "ni_heart_off.png";
            }
        }
        private async void SetLyric(string url)
        {
            try
            {
                string fileContent=null;
                lyrics = new List<LyricRow>();
                if (!url.StartsWith("http"))
                {
                    lyrics = FileRW.ReadLyric(url);

                    if (lyrics == null)
                        throw new Exception();
                    lv_lyrics.ItemsSource = lyrics;

                    return;
                }

                //Else
                var httpClient = new HttpClient();
                var response = await httpClient.GetAsync(url);
                response.EnsureSuccessStatusCode();
                fileContent = await response.Content.ReadAsStringAsync();

                List<string> lyricRows = fileContent.Split(new[] { Environment.NewLine }, StringSplitOptions.None).ToList();
                
                foreach(var r in lyricRows)
                {
                    string m = r.Substring(1, 5).Split(':')[0];
                    string s = r.Substring(1, 5).Split(':')[1];
                    int iminute = 0;
                    int isecond = 0;
                    Int32.TryParse(m, out iminute);
                    Int32.TryParse(s, out isecond);
                    int time = iminute * 60 + isecond;
                    LyricRow lyricRow = new LyricRow() { Content = r.Substring(10), Time = time };
                    lyrics.Add(lyricRow);    
                }
                lv_lyrics.ItemsSource = lyrics;
            }
            catch
            {
                List<LyricRow> lyrics = new List<LyricRow>();
                lyrics.Add(new LyricRow() { Time = 0, Content = "Không có lời bái hát!" });
                lv_lyrics.ItemsSource = lyrics;
            }
        }

        [Obsolete]
        private async void MenuItem_Download_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            string message = "";
            if (song.IsLocal)
            {
                message = "Bài hát đã được tải xuống.";
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
                return;
            }

            try
            {
                message = "Đang tải xuống bài hát " + song.Name;
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
                await new Downloader().SaveFilePublicStorageAsync(song.Name + "-" + song.ID + ".mp3", song.LinkMp3);
            }
            catch
            {
                message = "Tải xuống bài hát thất bại!";
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
            }
            try
            {
                await new Downloader().SaveFilePublicStorageAsync(song.Name + "-" + song.ID + ".lyric", song.LinkLyric);
            }
            catch { }

            message = "Đã tải xuống bài hát " + song.Name;
            Android.Widget.Toast.MakeText(
                Android.App.Application.Context,
                message,
                Android.Widget.ToastLength.Short).Show();
        }


        private void MenuItem_AddPlaylist_Clicked(object sender, EventArgs e)
        {
            playing_view.IsEnabled = false;
            playing_view.Opacity = 0.3;
            context_menu_add.IsVisible = true;
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            this.addingSong = song;
        }
        private Song addingSong = null;
        private void MenuItem_RemoveSong_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            int index = lvPlayingList.ItemsSource.Cast<Song>().ToList().IndexOf(song);
            try
            {
                CrossMediaManager.Current.Queue.MediaItems.RemoveAt(index);
            }
            catch { }
            
            vmPlayinglist.RemoveSong(vmPlayinglist.songs[index]);
            lvPlayingList.SetBinding(ListView.ItemsSourceProperty, "songs");
        }
        private async void btn_like_Pressed(object sender, EventArgs e)
        {
            if (vmPlayinglist.songs.Count == 0)
                return;
            Song song = (Song)vmPlayinglist.songs[playingIndex];
            if (!User.GetInstance().IsLoggedIn)
            {
                //Show hoi dang nhap
                bool answer = await DisplayAlert("Chưa đăng nhập", "Bạn có muốn đăng nhập?", "Vâng", "Lúc khác");
                if (answer)
                {
                    //Ensure Logout beforelogin
                    User.LogoutLocalUser();
                    await Navigation.PushModalAsync(new LoginPage());
                }
                return;
            }
            if (song.IsLocal)
            {
                // Toast bai hat offline
                string message = "Bài hát offline!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
                return;
            }
            if (isLiked)
            {
                isLiked = false;
                btn_like.Source = "ni_heart_off.png";
                //Call api dislike
                Song new_info = await DataService.LikeOrDisLikeAsync(isLiked, song);
                if (new_info != null)
                {
                    //Set label like, listen
                    lb_totalLike.Text = Song.TotalIntToString(Int32.Parse(new_info.TotalLike.ToString()));
                    lb_totalListen.Text = Song.TotalIntToString(Int32.Parse(new_info.TotalListen.ToString()));
                    //string message = "Đã bỏ thích!";
                    //Android.Widget.Toast.MakeText(Android.App.Application.Context,message,Android.Widget.ToastLength.Short).Show();
                }
            }
            else
            {
                isLiked = true;
                btn_like.Source = "ni_heart_fill.png";
                //Call api like
                Song new_info = await DataService.LikeOrDisLikeAsync(isLiked, song);
                if (new_info != null)
                {
                    //Set label like, listen
                    lb_totalLike.Text = Song.TotalIntToString(Int32.Parse(new_info.TotalLike.ToString()));
                    lb_totalListen.Text = Song.TotalIntToString(Int32.Parse(new_info.TotalListen.ToString()));
                    //string message = "Đã thích!";
                    //Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
                }
            }
        }

        private void progressBar_DragCompleted(object sender, EventArgs e)
        {
            CrossMediaManager.Current.Pause();
            int delta = (int)(CrossMediaManager.Current.Position.Ticks / 10000000) - (int)(progressBar.Value / 10000000);
            if (delta < 0)
            {
                CrossMediaManager.Current.StepSize = new TimeSpan(-delta * 10000000);
                CrossMediaManager.Current.StepForward();
            }
            //Lui ve sau
            else if (delta > 0)
            {
                CrossMediaManager.Current.StepSize = new TimeSpan(delta * 10000000);
                CrossMediaManager.Current.StepBackward();
            }
            CrossMediaManager.Current.Play();
        }

        #endregion

        private void btn_add_to_offline_playlist_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new LocalPlaylistPage(true, addingSong), true);
            btn_cancel_Clicked(sender, e);
        }

        private async void btn_add_to_online_playlist_Clicked(object sender, EventArgs e)
        {
            if (addingSong.IsLocal)
            {
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    "Không thể thêm bài hát trên máy vào danh sách online.",
                    Android.Widget.ToastLength.Short).Show();
                return;
            }
            if (!User.GetInstance().IsLoggedIn)
            {
                bool answer = await DisplayAlert("Chưa đăng nhập", "Bạn có muốn đăng nhập?", "Vâng", "Lúc khác");
                if (answer)
                {
                    //Ensure Logout beforelogin
                    User.LogoutLocalUser();
                    await Navigation.PushModalAsync(new LoginPage());
                }
                return;
            }
            await Navigation.PushAsync(new LocalPlaylistPage(true, addingSong, true), true);
            btn_cancel_Clicked(sender, e);
        }

        private void btn_cancel_Clicked(object sender, EventArgs e)
        {
            playing_view.IsEnabled = true;
            playing_view.Opacity = 1;
            context_menu_add.IsVisible = false;
        }
    }
}
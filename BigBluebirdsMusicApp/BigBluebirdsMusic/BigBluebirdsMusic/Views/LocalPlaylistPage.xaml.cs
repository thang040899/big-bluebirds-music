﻿using BigBluebirdsMusic.Models;
using BigBluebirdsMusic.Services;
using BigBluebirdsMusic.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BigBluebirdsMusic.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LocalPlaylistPage : ContentPage
    {
        private bool isAdd = false;
        private Song addingSong = null;
        private bool isOnlinePage = false;
        public LocalPlaylistPage(bool isAdd=false, Song addingSong=null, bool isOnlinePage=false)
        {
            InitializeComponent();
            
            if (isOnlinePage)
            {
                lb_title.Text = "Danh sách phát của tui";
                this.isOnlinePage = isOnlinePage;
            }
            this.isAdd = isAdd;
            if (isAdd)
            {
                lb_title.Text = "Thêm bài hát vào danh sách";
                this.addingSong = addingSong;
            }
        }
        private LocalDatabaseService database;
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            List<Playlist> lstPlaylist = new List<Playlist>();
            if (!isOnlinePage) // Load from local
            {
                database = new LocalDatabaseService();
                lstPlaylist = database.GetAllPlaylist();
            }
            else // Load from api
            {
                lstPlaylist = await DataService.GetOnlinePlaylistsOfUser();
            }
            vmListPlaylist = new ListPlaylistViewModel(lstPlaylist);
            clPlaylist.BindingContext = vmListPlaylist;
            clPlaylist.SetBinding(CollectionView.ItemsSourceProperty, "playlists");
        }

        public ListPlaylistViewModel vmListPlaylist;
        private void refresh_view_Refreshing(object sender, EventArgs e)
        {
            LocalDatabaseService database = new LocalDatabaseService();
            refresh_view.IsRefreshing = true;
            searchBar.Text = "";
            refresh_view.IsRefreshing = false;
        }

        private void clPlaylist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            var args = (TappedEventArgs)e;
            Playlist playlist = (Playlist)args.Parameter;
            // Console.WriteLine(((Playlist)args.Parameter).ToString());
            if (!isAdd) // Neu khong phai che do add
            {
                await Navigation.PushAsync(new MyPlaylistInfoPage(playlist), true);
                clPlaylist.SelectedItem = null;
                Console.WriteLine("AT: TAP PLAYLIST");
                //Code navigate
            }
            else // che Do add bai hat
            {
                if (!isOnlinePage)
                {
                    database.AddSongToPlaylist(addingSong, playlist);
                    Android.Widget.Toast.MakeText(
                        Android.App.Application.Context,
                        "Thêm bài hát vào danh sách phát thành công!",
                        Android.Widget.ToastLength.Short).Show();
                }
                else
                {
                    // Add song to online playlist
                    if(await DataService.AddSongToMyPlaylist(addingSong, playlist))
                        Android.Widget.Toast.MakeText(
                            Android.App.Application.Context,
                            "Thêm bài hát vào danh sách phát thành công!",
                            Android.Widget.ToastLength.Short).Show();
                    else
                        Android.Widget.Toast.MakeText(
                        Android.App.Application.Context,
                        "Thêm bài hát vào danh sách phát thất bại!",
                        Android.Widget.ToastLength.Short).Show();
                }
                
                await Navigation.PopAsync();
            }
        }

        private void btn_cancel_Clicked(object sender, EventArgs e)
        {
            list_playlist.Opacity = 1;
            list_playlist.IsEnabled = true;
            context_add_menu.IsVisible = false;
        }

        private async void btn_save_Clicked(object sender, EventArgs e)
        {
            //save 
            if(tbx_name_playlist.Text != "")
            {
                Playlist pll = new Playlist()
                {
                    ID = 0,
                    DateCreate = DateTime.Now.ToString("dd/MM/yyyy"),
                    IsLocal = !isOnlinePage,
                    NumberSong = 0,
                    Name = tbx_name_playlist.Text,
                    Description = tbx_description_playlist.Text,
                    ImageLink = "music_playlist.jpg",
                    TotalLike = 0,
                    TotalListen = 0,
                };
                if (!isOnlinePage)
                {
                    new LocalDatabaseService().AddPlaylist(pll);
                    Android.Widget.Toast.MakeText(
                            Android.App.Application.Context,
                            "Tạo danh sách phát thành công!",
                            Android.Widget.ToastLength.Short).Show();
                    vmListPlaylist.AddPlaylist(pll);
                }
                else // online
                {
                    if (await DataService.CreatePlaylist(pll))
                    {
                        Android.Widget.Toast.MakeText(
                            Android.App.Application.Context,
                            "Tạo danh sách phát thành công!",
                            Android.Widget.ToastLength.Short).Show();
                        vmListPlaylist.AddPlaylist(pll);
                    }
                    else
                    {
                        Android.Widget.Toast.MakeText(
                            Android.App.Application.Context,
                            "Tạo danh sách phát thất bại!",
                            Android.Widget.ToastLength.Short).Show();
                    }
                }
                
                
            }
            
            //exit
            btn_cancel_Clicked(sender, e);
        }

        private void btn_add_playlist_Clicked(object sender, EventArgs e)
        {
            list_playlist.Opacity = 0.5;
            list_playlist.IsEnabled = false;
            context_add_menu.IsVisible = true;
        }

        private void searchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Playlist> lstPlaylist=new List<Playlist>();
            if (searchBar.Text != "")
            {

                if (!isOnlinePage)
                {
                    lstPlaylist = database.GetAllPlaylist(searchBar.Text);
                    vmListPlaylist = new ListPlaylistViewModel(lstPlaylist);
                }
                else
                {
                    lstPlaylist = vmListPlaylist.allPlaylists.Where(x => x.Name.Contains(searchBar.Text)).ToList();
                    vmListPlaylist = new ListPlaylistViewModel(lstPlaylist, vmListPlaylist.allPlaylists.ToList());
                }
                
            }
            else // restore search status
            {
                if (!isOnlinePage)
                {
                    lstPlaylist = database.GetAllPlaylist();
                    vmListPlaylist = new ListPlaylistViewModel(lstPlaylist);
                }
                else
                {
                    vmListPlaylist = new ListPlaylistViewModel(vmListPlaylist.allPlaylists.ToList(), vmListPlaylist.allPlaylists.ToList());
                }
            }
            clPlaylist.BindingContext = vmListPlaylist;
            clPlaylist.SetBinding(CollectionView.ItemsSourceProperty, "playlists");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.FilePicker;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using MediaManager.Forms;
using BigBluebirdsMusic.ViewModels;
using BigBluebirdsMusic.Services;
using Plugin.FilePicker.Abstractions;
using BigBluebirdsMusic.Models;
using MediaManager;

namespace BigBluebirdsMusic.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MySongsOnlinePage : ContentPage
    {
        private UserOnlineSongsViewModel vmMySongsOnlinePage;
        public MySongsOnlinePage()
        {
            InitializeComponent();
            vmMySongsOnlinePage = UserOnlineSongsViewModel.GetInstance();
            this.BindingContext = vmMySongsOnlinePage;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            vmMySongsOnlinePage.GetSongsOfCurrentUserCommand.Execute(null);
        }


        private async void btn_add_song_Clicked(object sender, EventArgs e)
        {
            vmMySongsOnlinePage.IsAddingOrEditing = true;
            list_song.Opacity = 0.5;
            list_song.IsEnabled = false;
            context_add_menu.IsVisible = true;
            btn_deleteSong.IsVisible = false;
            txt_context_add_edit.Text = "Thêm mới";

            //var a = await CrossFilePicker.Current.PickFile();
            //string contents = System.Text.Encoding.UTF8.GetString(a.DataArray);
            //var content = new MultipartFormDataContent();

            //// Add the image:
            //MemoryStream stream = new MemoryStream(a.DataArray);
            //var imageContent = new StreamContent(a.GetStream());
            //imageContent.Headers.ContentType = new MediaTypeHeaderValue("image/png");
            //content.Add(imageContent, "Thumbnail", "TestImageeee.png");

            //// Add another parameter:
            //content.Add(new StringContent("Name a"), "Name");
            //content.Add(new StringContent("Description a"), "Description");
            //var url = "http://192.168.1.135:5000/api/Songs?Id=1179";
            //var client = new HttpClient();
            //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + BigBluebirdsMusic.Models.User.GetInstance().Token);
            //var response = await client.PutAsync(url, content);
        }

        private void searchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            vmMySongsOnlinePage.GetSongsOfCurrentUserCommand.Execute(searchBar.Text);
        }

        private void refresh_view_Refreshing(object sender, EventArgs e)
        {
            vmMySongsOnlinePage.GetSongsOfCurrentUserCommand.Execute(null);
            refresh_view.IsRefreshing = false;
        }

        private void lvListMySongs_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Song song = (Song)e.Item;
            PlayingPage.GetInstance().PlaySong(song);
        }

        private async void MenuItem_Download_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            string message = "";
            if (song.IsLocal)
            {
                message = "Bài hát đã được tải xuống.";
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
                return;
            }

            try
            {
                message = "Đang tải xuống bài hát " + song.Name;
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
                await new Downloader().SaveFilePublicStorageAsync(song.Name + "-" + song.ID + ".mp3", song.LinkMp3);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                message = "Tải xuống bài hát thất bại!";
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
                return;
            }
            try
            {
                await new Downloader().SaveFilePublicStorageAsync(song.Name + "-" + song.ID + ".lyric", song.LinkLyric);
            }
            catch { }

            message = "Đã tải xuống bài hát " + song.Name;
            Android.Widget.Toast.MakeText(
                Android.App.Application.Context,
                message,
                Android.Widget.ToastLength.Short).Show();
        }

        private async void MenuItem_AddPlaylist_Clicked(object sender, EventArgs e)
        {
            if (!User.GetInstance().IsLoggedIn)
            {
                bool answer = await DisplayAlert("Chưa đăng nhập", "Bạn có muốn đăng nhập?", "Vâng", "Lúc khác");
                if (answer)
                {
                    //Ensure Logout beforelogin
                    User.LogoutLocalUser();
                    await Navigation.PushModalAsync(new LoginPage());
                }
                return;
            }
            var menuItem = ((MenuItem)sender);
            Song addingSong = (Song)menuItem.CommandParameter;
            await Navigation.PushAsync(new LocalPlaylistPage(true, addingSong, true), true);
        }

        private void MenuItem_AddPlayingList_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            try
            {
                MediaManager.Library.MediaItem mediaItem = new MediaManager.Library.MediaItem(song.LinkMp3);
                CrossMediaManager.Current.Queue.Add(mediaItem);
                CrossMediaManager.Current.Queue.MediaItems.Add(mediaItem);
                PlayingPage.GetInstance().vmPlayinglist.AddSong(song);
                string message = "Đã thêm bài hát vào danh sách đang phát!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
            }
            catch
            {
                string message = "Thêm bài hát vào danh sách đang phát thất bại!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
            }
        }

        private void btn_cancel_Clicked(object sender, EventArgs e)
        {
            list_song.Opacity = 1;
            list_song.IsEnabled = true;
            context_add_menu.IsVisible = false;
            selectedImageData = null;
            selectedSongData = null;
            editingSong = null;
            lb_avatar.Text = "";
            lb_selectedSong.Text = "";
            tbx_name_song.Text = "";
        }

        private async void btn_save_Clicked(object sender, EventArgs e)
        {
            if (tbx_name_song.Text == "" || tbx_name_song.Text == null)
            {
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    "Vui lòng nhập tên bài hát!",
                    Android.Widget.ToastLength.Short).Show();
                return;
            }
            if ((selectedImageData == null || lb_avatar.Text == "") && vmMySongsOnlinePage.IsAddingOrEditing)
            {
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    "Vui lòng chọn ảnh bìa cho bài hát!",
                    Android.Widget.ToastLength.Short).Show();
                return;
            }
            if ((selectedSongData == null || lb_selectedSong.Text == "")&& vmMySongsOnlinePage.IsAddingOrEditing)
            {
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    "Vui lòng chọn tệp bài hát!",
                    Android.Widget.ToastLength.Short).Show();
                return;
            }

            if (vmMySongsOnlinePage.IsAddingOrEditing)
            {
                // disable first
                btn_save.IsEnabled = false;
                btn_cancel.IsVisible = false;
                btn_selectImage.IsEnabled = false;
                btn_selectSong.IsEnabled = false;
                btn_save.Text = "Đang đồng bộ...";
                btn_save.WidthRequest = 180;
                // Save song
                bool result = await DataService.CreateSong(
                    tbx_name_song.Text, "Bài hát "+ tbx_name_song.Text, selectedImageData, selectedSongData);

                // reenable
                btn_save.IsEnabled = true;
                btn_cancel.IsVisible = true;
                btn_selectImage.IsEnabled = true;
                btn_selectSong.IsEnabled = true;
                btn_save.Text = "Lưu";
                btn_save.WidthRequest = 60;
                if (!result)
                {
                    Android.Widget.Toast.MakeText(
                        Android.App.Application.Context,
                        "Tải lên bài hát thất bại!",
                        Android.Widget.ToastLength.Short).Show();
                    return;
                }

                Android.Widget.Toast.MakeText(
                        Android.App.Application.Context,
                        "Tải lên bài hát thành công!",
                        Android.Widget.ToastLength.Short).Show();
                vmMySongsOnlinePage.GetSongsOfCurrentUserCommand.Execute(null);
            }
            else
            {
                if(editingSong == null)
                {
                    Android.Widget.Toast.MakeText(
                           Android.App.Application.Context,
                           "Vui lòng chọn bài hát để chỉnh sửa!",
                           Android.Widget.ToastLength.Short).Show();
                    return;
                }
                // disable first
                btn_save.IsEnabled = false;
                btn_cancel.IsVisible = false;
                btn_selectImage.IsEnabled = false;
                btn_selectSong.IsEnabled = false;
                btn_save.Text = "Đang đồng bộ...";
                btn_save.WidthRequest = 180;
                // Edit song
                bool result = await DataService.UpdateSong(
                    editingSong.ID, tbx_name_song.Text, "Bài hát "+ tbx_name_song.Text, selectedImageData);
                
                // reenable
                btn_save.IsEnabled = true;
                btn_cancel.IsVisible = true;
                btn_selectImage.IsEnabled = true;
                btn_selectSong.IsEnabled = true;
                btn_save.Text = "Lưu";
                btn_save.WidthRequest = 60;
                if (!result)
                {
                    Android.Widget.Toast.MakeText(
                        Android.App.Application.Context,
                        "Chỉnh sửa bài hát thất bại!",
                        Android.Widget.ToastLength.Short).Show();
                    return;
                }

                Android.Widget.Toast.MakeText(
                        Android.App.Application.Context,
                        "Chỉnh sửa bài hát thành công!",
                        Android.Widget.ToastLength.Short).Show();
                vmMySongsOnlinePage.GetSongsOfCurrentUserCommand.Execute(null);
            }
            btn_cancel_Clicked(sender, e);
        }

        FileData selectedImageData = null;
        FileData selectedSongData = null;
        private async void btn_selectImage_Clicked(object sender, EventArgs e)
        {
            try
            {
                selectedImageData = await FileRW.PickImageFile();
                lb_avatar.Text = selectedImageData.FileName;
            }
            catch { }
        }

        private async void btn_selectSong_Clicked(object sender, EventArgs e)
        {
            try
            {
                selectedSongData = await FileRW.PickMusicFile();
                lb_selectedSong.Text = selectedSongData.FileName;
            }
            catch { }
        }

        private Song editingSong = null;
        private void MenuItem_Edit_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            editingSong = (Song)menuItem.CommandParameter;
            vmMySongsOnlinePage.IsAddingOrEditing = false;
            txt_context_add_edit.Text = "Chỉnh sửa";
            list_song.Opacity = 0.5;
            list_song.IsEnabled = false;
            context_add_menu.IsVisible = true;
            btn_deleteSong.IsVisible = true;
            tbx_name_song.Text = editingSong.Name;
        }

        private async void btn_deleteSong_Clicked(object sender, EventArgs e)
        {
            if(editingSong == null)
            {
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    "Vui lòng chọn bài hát để xóa!",
                    Android.Widget.ToastLength.Short).Show();
                return;
            }
            bool answer = await DisplayAlert(
                "Xóa", "Bạn có muốn xóa bài hát ["+ editingSong.Name+ "]?",
                "Vâng", "Lúc khác");
            if (!answer)
            {
                return;
            }
            bool res = await DataService.DeleteSong(editingSong.ID);

            if (!res)
            {
                Android.Widget.Toast.MakeText(
                        Android.App.Application.Context,
                        "Xóa bài hát thất bại!",
                        Android.Widget.ToastLength.Short).Show();
                return;
            }
            Android.Widget.Toast.MakeText(
                        Android.App.Application.Context,
                        "Xóa bài hát thành công!",
                        Android.Widget.ToastLength.Short).Show();
            vmMySongsOnlinePage.GetSongsOfCurrentUserCommand.Execute(null);
            btn_cancel_Clicked(sender, e);
        }
    }
}
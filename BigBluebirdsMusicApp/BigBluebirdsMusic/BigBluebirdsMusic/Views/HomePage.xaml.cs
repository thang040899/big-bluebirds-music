﻿using BigBluebirdsMusic.Models;
using BigBluebirdsMusic.Services;
using BigBluebirdsMusic.ViewModels;
using MediaManager;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BigBluebirdsMusic.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        HomePageViewModel vmHomePage;
        public HomePage()
        {
            vmHomePage = HomePageViewModel.GetInstance();
            InitializeComponent();
            this.BindingContext = vmHomePage;
        }
        private bool isLoaded = false;
        protected override void OnAppearing()
        {
            base.OnAppearing();
            InitListRecentSongs();
            InitListForYou();
            InitListPlayListsRecommended();
            //Load only one time when start
            if (!isLoaded)
            {
                InitPanelImage();
                InitListTopSongs();
                isLoaded = true;
            }
            
        }
        public void InitPanelImage()
        {
            var listPanelImage = new List<string>
            {
                "bbb_main.png",
                "music_theme.jpg",
                "cover_love_music.jpg",
            };
            ThePanel.ItemsSource = listPanelImage;

            var animation = new Animation(v => {
                if(ThePanel.Position == listPanelImage.Count-1)
                    ThePanel.ScrollTo(0, animate: false);
                else
                    ThePanel.ScrollTo(ThePanel.Position + 1, animate: true);
            });
            // 5s chuyen canh, 15s ket thuc
            animation.Commit(this, "PanelAnimation", 5000, 15000, Easing.Linear, (v, c) => ThePanel.ScrollTo(0, animate: true) , () => true);
        }
    
        public void InitListRecentSongs()
        {
            vmHomePage.GetRecentSongsCommand.Execute(null);
        }

        public void InitListForYou()
        {
            vmHomePage.GetRecommendSongsCommand.Execute(null);

        }

        public void InitListTopSongs()
        {
            vmHomePage.GetTopSongsCommand.Execute(null);
        }

        public void InitListPlayListsRecommended()
        {
            vmHomePage.GetRecommendPlaylistsCommand.Execute(null);
        }


        private async void clPlaylist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (clPlaylist.SelectedItem != null)
            {
                Playlist playlist = (Playlist)clPlaylist.SelectedItem;
                await Navigation.PushAsync(new PlaylistInfoPage(playlist));
                clPlaylist.SelectedItem = null;
                Console.WriteLine("AT: TAP PLAYLIST");
                //Code navigate
            }
        }

        private void lvListRecentSongs_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Song song = (Song)e.Item;
            PlayingPage.GetInstance().PlaySong(song);
        }

        private void lvListTopSongs_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Song song = (Song)e.Item;
            PlayingPage.GetInstance().PlaySong(song);
        }

        [Obsolete]
        private async void MenuItem_Download_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            string message = "";
            if (song.IsLocal)
            {
                message = "Bài hát đã được tải xuống.";
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
                return;
            }
                
            try
            {
                message = "Đang tải xuống bài hát "+song.Name;
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
                await new Downloader().SaveFilePublicStorageAsync(song.Name + "-" + song.ID + ".mp3", song.LinkMp3);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                message = "Tải xuống bài hát thất bại!";
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
                return;
            }
            try
            {
                await new Downloader().SaveFilePublicStorageAsync(song.Name + "-" + song.ID + ".lyric", song.LinkLyric);
            }
            catch { }

            message = "Đã tải xuống bài hát "+song.Name;
            Android.Widget.Toast.MakeText(
                Android.App.Application.Context,
                message,
                Android.Widget.ToastLength.Short).Show();
        }

        private void MenuItem_AddPlaylist_Clicked(object sender, EventArgs e)
        {
            refresh_view.IsEnabled = false;
            refresh_view.Opacity = 0.3;
            context_menu_add.IsVisible = true;
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            this.addingSong = song;
        }
        private Song addingSong = null;
        private void refresh_view_Refreshing(object sender, EventArgs e)
        {
            Refresh();
            refresh_view.IsRefreshing = false;
        }

        private void Refresh()
        {
            InitListRecentSongs();
            InitListTopSongs();
            InitListForYou();
            InitListPlayListsRecommended();
        }

        private void MenuItem_AddPlayingList_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            try
            {
                MediaManager.Library.MediaItem mediaItem = new MediaManager.Library.MediaItem(song.LinkMp3);
                CrossMediaManager.Current.Queue.Add(mediaItem);
                CrossMediaManager.Current.Queue.MediaItems.Add(mediaItem);
                PlayingPage.GetInstance().vmPlayinglist.AddSong(song);
                string message = "Đã thêm bài hát vào danh sách đang phát!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
            }
            catch {
                string message = "Thêm bài hát vào danh sách đang phát thất bại!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
            }

        }

        private void btn_add_to_offline_playlist_Clicked(object sender, EventArgs e)
        {
            
            Navigation.PushAsync(new LocalPlaylistPage(true, addingSong), true);
            btn_cancel_Clicked(sender, e);
        }

        private async void btn_add_to_online_playlist_Clicked(object sender, EventArgs e)
        {
            if (addingSong.IsLocal)
            {
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    "Không thể thêm bài hát trên máy vào danh sách online.",
                    Android.Widget.ToastLength.Short).Show();
                return;
            }
            if (!User.GetInstance().IsLoggedIn)
            {
                bool answer = await DisplayAlert("Chưa đăng nhập", "Bạn có muốn đăng nhập?", "Vâng", "Lúc khác");
                if (answer)
                {
                    //Ensure Logout beforelogin
                    User.LogoutLocalUser();
                    await Navigation.PushModalAsync(new LoginPage());
                }
                return;
            }
            await Navigation.PushAsync(new LocalPlaylistPage(true, addingSong, true), true);
            btn_cancel_Clicked(sender, e);
        }

        private void btn_cancel_Clicked(object sender, EventArgs e)
        {
            refresh_view.IsEnabled = true;
            refresh_view.Opacity = 1;
            context_menu_add.IsVisible = false;
        }

    }
}
﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Xamarin.Forms.PlatformConfiguration;
using TabbedPage = Xamarin.Forms.TabbedPage;
using BigBluebirdsMusic.Models;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

namespace BigBluebirdsMusic.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : TabbedPage
    {
        [Obsolete]
        public MainPage()
        {
            InitializeComponent();
            On<Xamarin.Forms.PlatformConfiguration.Android>().SetToolbarPlacement(ToolbarPlacement.Bottom);

            NavigationPage homePage = new NavigationPage(new HomePage());
            homePage.IconImageSource = "ihome.png";
            homePage.Title = "Home";
            Children.Add(homePage);

            NavigationPage searchPage = new NavigationPage(new SearchPage());
            searchPage.IconImageSource = "isearch.png";
            searchPage.Title = "Search";
            Children.Add(searchPage);

            NavigationPage playerPage = new NavigationPage(PlayingPage.GetInstance());
            playerPage.IconImageSource = "iplay.png";
            playerPage.Title = "Player";
            Children.Add(playerPage);

            NavigationPage localPage = new NavigationPage(new LocalPage());
            localPage.IconImageSource = "iprofile.png";
            localPage.Title = "Library";
            Children.Add(localPage);

            NavigationPage profilePage = new NavigationPage(new ProfilePage());
            profilePage.IconImageSource = "ni_user_white.png";
            profilePage.Title = "Profile";
            Children.Add(profilePage);

            //NavigationPage localMusicPage = new NavigationPage(new RegisterPage());
            //localMusicPage.IconImageSource = "iprofile.png";
            //localMusicPage.Title = "TEST";
            //Children.Add(localMusicPage);

            CheckPermission();
        }

        [Obsolete]
        public async void CheckPermission()
        {
            var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Storage });
            var status = results[Permission.Storage];
            if (status.Equals(PermissionStatus.Granted))
            {

            }
        }
    }
}
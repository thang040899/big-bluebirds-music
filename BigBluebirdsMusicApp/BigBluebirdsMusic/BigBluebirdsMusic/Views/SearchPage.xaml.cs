﻿using BigBluebirdsMusic.Models;
using BigBluebirdsMusic.Services;
using BigBluebirdsMusic.ViewModels;
using MediaManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BigBluebirdsMusic.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchPage : ContentPage
    {
        private SearchPageViewModel vmSearchPage;
        public SearchPage()
        {
            InitializeComponent();
            searchBar.TextChanged += OnTextChanged;
            vmSearchPage = SearchPageViewModel.GetInstance();
            this.BindingContext = vmSearchPage;
        }

        private async void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            SearchBar searchBar = (SearchBar)sender;
            if (searchBar.Text.Length == 0)
            {
                vmSearchPage.ResetSearchResultCommand.Execute(null);
                return;
            }
            vmSearchPage.GetSearchResultCommand.Execute(searchBar.Text);
            searchResults.HeightRequest = 1 * 60 + 20;
            //clPlaylist.HeightRequest = (1/2+1) * 220 + 30;
            //clPlaylist.HeightRequest = 10;
        }

        private void clPlaylist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (clPlaylist.SelectedItem != null)
            {
                Navigation.PushAsync(new PlaylistInfoPage((Playlist)clPlaylist.SelectedItem));
                clPlaylist.SelectedItem = null;
                Console.WriteLine("AT: TAP PLAYLIST");
                //Code navigate
            }
        }

        private void searchResults_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Song song = (Song)e.Item;
            PlayingPage.GetInstance().PlaySong(song);
        }

        [Obsolete]
        private async void MenuItem_Download_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            string message = "";
            if (song.IsLocal)
            {
                message = "Bài hát đã được tải xuống.";
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
                return;
            }

            try
            {
                message = "Đang tải xuống bài hát " + song.Name;
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
                await new Downloader().SaveFilePublicStorageAsync(song.Name + "-" + song.ID + ".mp3", song.LinkMp3);
            }
            catch
            {
                message = "Tải xuống bài hát thất bại!";
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    message,
                    Android.Widget.ToastLength.Short).Show();
            }
            try
            {
                await new Downloader().SaveFilePublicStorageAsync(song.Name + "-" + song.ID + ".lyric", song.LinkLyric);
            }
            catch { }

            message = "Đã tải xuống bài hát " + song.Name;
            Android.Widget.Toast.MakeText(
                Android.App.Application.Context,
                message,
                Android.Widget.ToastLength.Short).Show();
        }

        private void MenuItem_AddPlaylist_Clicked(object sender, EventArgs e)
        {
            search_view.IsEnabled = false;
            search_view.Opacity = 0.3;
            context_menu_add.IsVisible = true;
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            this.addingSong = song;
        }
        private Song addingSong = null;
        private void MenuItem_AddPlayingList_Clicked(object sender, EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            Song song = (Song)menuItem.CommandParameter;
            try
            {
                MediaManager.Library.MediaItem mediaItem = new MediaManager.Library.MediaItem(song.LinkMp3);
                CrossMediaManager.Current.Queue.Add(mediaItem);
                CrossMediaManager.Current.Queue.MediaItems.Add(mediaItem);
                PlayingPage.GetInstance().vmPlayinglist.AddSong(song);
                string message = "Đã thêm bài hát vào danh sách đang phát!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
            }
            catch
            {
                string message = "Thêm bài hát vào danh sách đang phát thất bại!";
                Android.Widget.Toast.MakeText(Android.App.Application.Context, message, Android.Widget.ToastLength.Short).Show();
            }

        }

        private void btn_add_to_offline_playlist_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new LocalPlaylistPage(true, addingSong), true);
            btn_cancel_Clicked(sender, e);
        }

        private async void btn_add_to_online_playlist_Clicked(object sender, EventArgs e)
        {
            if (addingSong.IsLocal)
            {
                Android.Widget.Toast.MakeText(
                    Android.App.Application.Context,
                    "Không thể thêm bài hát trên máy vào danh sách online.",
                    Android.Widget.ToastLength.Short).Show();
                return;
            }
            if (!User.GetInstance().IsLoggedIn)
            {
                bool answer = await DisplayAlert("Chưa đăng nhập", "Bạn có muốn đăng nhập?", "Vâng", "Lúc khác");
                if (answer)
                {
                    //Ensure Logout beforelogin
                    User.LogoutLocalUser();
                    await Navigation.PushModalAsync(new LoginPage());
                }
                return;
            }
            await Navigation.PushAsync(new LocalPlaylistPage(true, addingSong, true), true);
            btn_cancel_Clicked(sender, e);
        }

        private void btn_cancel_Clicked(object sender, EventArgs e)
        {
            search_view.IsEnabled = true;
            search_view.Opacity = 1;
            context_menu_add.IsVisible = false;
        }

        private ISpeechToText _speechRecongnitionInstance;
        private async void btn_micSearch_Clicked(object sender, EventArgs e)
        {
            try
            {
                _speechRecongnitionInstance = DependencyService.Get<ISpeechToText>();
            }
            catch (Exception ex)
            {
            }
            var speechText = await WaitForSpeechToText();
            searchBar.Text = string.IsNullOrEmpty(speechText) ? "" : speechText;
        }
        async Task<string> WaitForSpeechToText()
        {
            try
            {
                _speechRecongnitionInstance = DependencyService.Get<ISpeechToText>();
                return await _speechRecongnitionInstance.SpeechToTextAsync();
            }
            catch { }
            return "";
        }
    }
}
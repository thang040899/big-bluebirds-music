export interface Owner {
    ownerId: string;
    nameOwner: string;
    thumbnail: string;
}

export interface ISong {
    id: number;
    name: string;
    description: string;
    dateCreate: Date;
    totalListen: number;
    totalLike: number;
    totalCmt: number;
    totalDownload: number;
    lyric: string;
    thumbnail: string;
    fileMusic: string;
    duration: number;
    types: any[];
    tags: any[];
    owners: Owner[];
}
export class Song implements ISong {
    id!: number;
    name!: string;
    description!: string;
    dateCreate!: Date;
    totalListen!: number;
    totalLike!: number;
    totalCmt!: number;
    totalDownload!: number;
    lyric!: string;
    thumbnail!: string;
    fileMusic!: string;
    duration!: number;
    types!: any[];
    tags!: any[];
    owners!: Owner[];
    init(data?: any) {
        if (data) {
            this.id = data['id'];
            this.name = data['name'];
            this.description = data['description'];
            this.dateCreate = data['dateCreate'];
            this.totalListen = data['totalListen'];
            this.totalLike = data['totalLike'];
            this.totalCmt = data['totalCmt'];
            this.totalDownload = data['totalDownload'];
            this.totalLike = data['totalLike'];
            this.thumbnail = data['thumbnail'];
            this.fileMusic = data['fileMusic'];
            this.types = data['types'];
            this.tags = data['tags'];
            this.owners = data['owners'];
        }
    }
    static fromJS(data: any): Song {
        data = typeof data === 'object' ? data : {};
        let result = new Song();
        result.init(data);
        return result;
    }

}
export interface StreamState {
    playing: boolean;
    readableCurrentTime: string;
    readableDuration: string;
    duration: number | undefined;
    currentTime: number | undefined;
    volume: number | undefined;
    canplay: boolean;
    error: boolean;
}
export class PlayList {
    id: number;
    name: string;
    description: string;
    dateCreate: Date;
    totalSong: number;
    totalListen: number;
    totalLike: number;
    totalCmt: number;
    thumbnail: string;
    playlistType: string;
    owners: Owner[];

}
import { Injectable } from '@angular/core';
import { StreamState } from 'share/model';
import { AudioService } from 'app/services/audio-service';
import { PlayListsServiceProxy } from 'share/services'
@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  files: Array<any> = [
  ];
  state: StreamState;
  currentFile: any = {};
  constructor(
    public audioService: AudioService,
    public playlistservice: PlayListsServiceProxy

  ) {
  }
  setList(list: any[]) {
    this.files = list;
    // listen to stream state
    this.audioService.getState().subscribe(state => {
      this.state = state;
    });
  }
  ngOnInit() {
  }
  playStream(url) {
    this.audioService.playStream(url).subscribe(events => {
      // listening for fun here
    });
  }
  openFile(file, index) {
    this.currentFile = { index, file };
    this.audioService.stop();
    this.playStream(file.fileMusic);
  }
  pause() {
    this.audioService.pause();
  }
  play() {
    this.audioService.play();
  }
  stop() {
    this.audioService.stop();
  }
  next() {
    const index = this.currentFile.index + 1;
    const file = this.files[index];
    this.openFile(file, index);
  }
  previous() {
    const index = this.currentFile.index - 1;
    const file = this.files[index];
    this.openFile(file, index);
  }
  isFirstPlaying() {
    return this.currentFile.index === 0;
  }
  isLastPlaying() {
    return this.currentFile.index === this.files.length - 1;
  }
  onSliderChangeEnd(change) {
    this.audioService.seekTo(change.value);
  }
  onVolumeChange(event) {

    this.audioService.setVolume(event.value);
  }
  PlayListToFooter(files: any[]) {
    return files;
  }
}



import { Component, OnInit } from '@angular/core';
import { Router, Routes } from '@angular/router'
import { ApiServiceProxy } from "../../share/services"
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css', '../../styles.css']
})
export class DashboardComponent implements OnInit {

  constructor(public playlistAPI: ApiServiceProxy, public routerService: Router) { }

  ngOnInit() {
    /* ----------==========     Daily Sales Chart initialization For Documentation    ==========---------- */

  }
  navigate(url: string) {
    //this.routerService.navigate([url]);
    this.routerService.navigateByUrl(url);
  }
}

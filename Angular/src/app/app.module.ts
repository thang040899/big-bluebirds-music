import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { API_BASE_URL } from '../share/services';
//import { AbpModule } from '@abp/abp.module';
//import { AbpHttpInterceptor } from '@abp/abpHttpInterceptor';

import { ApiServiceProxy } from 'share/services'
import { Song } from 'share/model'
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LoginComponent } from '../account/login/login.component'
import { RegisterComponent } from '../account/register/register.component';


@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,

  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoginComponent,
    RegisterComponent,

  ],
  providers: [
    ApiServiceProxy,
    {
      provide: API_BASE_URL,
      useValue: environment.apiRoot
      // { provide: HTTP_INTERCEPTORS, useClass: AbpHttpInterceptor, multi: true },
      // { provide: API_BASE_URL, useFactory: getRemoteServiceBaseUrl },
      // {
      //   provide: APP_INITIALIZER,
      //   useFactory: appInitializerFactory,
      //   deps: [Injector, PlatformLocation],
      //   multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

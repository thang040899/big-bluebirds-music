import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { PlayerComponent } from './player/player.component';

import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatSliderModule } from '@angular/material/slider';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { PlayListsServiceProxy } from 'share/services';
import { PlayerService } from 'app/services/player.service';

const modules = [
  MatButtonModule,
  MatListModule,
  MatSliderModule,
  MatIconModule,
  MatToolbarModule,
  MatCardModule
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    modules
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    PlayerComponent
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    PlayerComponent,
    modules,

  ]
  , providers: [
    PlayerComponent,
    PlayListsServiceProxy,
    PlayerService
  ]
})
export class ComponentsModule { }

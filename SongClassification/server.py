import flask
from saved_model import *
from flask import request
import logging


app = flask.Flask(__name__)
app.config["DEBUG"] = False
logging.basicConfig(filename='app.log',level=logging.INFO)

@app.route('/api/get-song-type/', methods=['POST'])
def api_get_song_type():
    logging.info("Get song type start.")
    logging.info(json.loads(request.data))
    file_name = json.loads(request.data)['file_name']
    return get_song_type(file_name)

app.run(host = "127.0.0.1", port = 9566)

from tensorflow import keras
import numpy as np
from functions import *
from collections import Counter
from np_encoder import NpEncoder
import logging


logging.basicConfig(filename='app.log',level=logging.INFO)
TAGS = ['disco', 'reggae', 'rock', 'pop', 'blues', 'country', 'jazz', 'classical', 'metal', 'hiphop']

logging.info('Model loading...')
model = keras.models.load_model('model_cnn1')
logging.info('MODEL LOADED!')

def predict(X):
    # perform prediction
    prediction = model.predict(X)
    # get index with max value
    predicted_index = np.argmax(prediction, axis=1)
    return predicted_index

def get_song_type(file_full_name):
    # file_path = r'https://c1-ex-swe.nixcdn.com/NhacCuaTui035/Ikissedthegirl-KatyPerry_5srj.mp3?st=-STwBtQulZsCF3_lF03uCw&e=1617519228&t=1617432828645'
    file_name = file_full_name.replace(".mp3", "")
    mfcc_data_1 = cut_and_convert(file_full_name, file_name, 30)
    result_array = predict(mfcc_data_1)
    result_counter = Counter(result_array)
    logging.info(result_counter)
    result = []
    for item in result_counter.keys():
        res_data = {
            "tag" : TAGS[item],
            "key" : item,
            "value" : result_counter.get(item)
        }
        result.append(res_data)
    result.sort(key=lambda dic: dic['value'], reverse=True)
    response = {
        "result": result
    }
    return json.dumps(response, cls=NpEncoder)

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class InitDB4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "Users",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 802, DateTimeKind.Local).AddTicks(3569),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 879, DateTimeKind.Local).AddTicks(376));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Like_Song",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 838, DateTimeKind.Local).AddTicks(4118),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 911, DateTimeKind.Local).AddTicks(5381));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Cmt_Song",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 840, DateTimeKind.Local).AddTicks(8372),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 913, DateTimeKind.Local).AddTicks(8063));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "Songs",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 824, DateTimeKind.Local).AddTicks(5654),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 897, DateTimeKind.Local).AddTicks(9560));

            migrationBuilder.AddColumn<bool>(
                name: "IsPublic",
                table: "Songs",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "PlayList",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 828, DateTimeKind.Local).AddTicks(1790),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 901, DateTimeKind.Local).AddTicks(1952));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateSendRequest",
                table: "Friends",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 842, DateTimeKind.Local).AddTicks(9877),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 916, DateTimeKind.Local).AddTicks(32));

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                column: "ConcurrencyStamp",
                value: "dbc5cdf4-396c-402c-b9fc-3850f92b16f7");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                column: "ConcurrencyStamp",
                value: "a9efa616-8d51-4624-9348-5277295fcac1");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                column: "ConcurrencyStamp",
                value: "0c7eb8ac-826f-46a8-8180-465594f8e74f");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                column: "ConcurrencyStamp",
                value: "2d32fb20-65a2-49f1-9690-0ec6a0387691");

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(9969));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 914, DateTimeKind.Local).AddTicks(824));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 914, DateTimeKind.Local).AddTicks(840));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 914, DateTimeKind.Local).AddTicks(842));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 914, DateTimeKind.Local).AddTicks(843));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(2414));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(6931));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7026));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7030));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7033));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7036));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7038));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7041));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7043));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7046));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7048));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7051));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7053));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreate",
                value: new DateTime(2020, 12, 27, 9, 58, 32, 913, DateTimeKind.Local).AddTicks(7056));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "7eaaa837-b7be-497c-9ab0-2bf6307747cf", "AQAAAAEAACcQAAAAEL0hHuUVvY5o1UgtV2Kxo8H8X3pCZO5rxichILJyMiNDqJnyd67AG169Ys26AjyFpA==", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "15b79dc4-ad3f-446b-8fa5-3a9bd4593aa6", "AQAAAAEAACcQAAAAEOsFDuDT3Nic+hmtMbmeqSitZfx4qHOewPj3vKFJLDunrxVrI1lJXM+M61+EDdFffg==", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "84fcf361-0764-4bf2-bbb6-3323bc1d97b1", "AQAAAAEAACcQAAAAEDawYAbv2ph7FW59OB7DVd3MHzph1ruxK5PBd3QPsa3tsCHN4/bnFGFPL2XYghVMYw==", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "cb7dfb41-6ab1-4104-be1c-f65ea9ea012b", "AQAAAAEAACcQAAAAEOS5MSAvKCIe3jmorWsGcLKwCrOCgps+Atw1bUmaKL78r6lF/Uy0Lsrckw0sfgpr+A==", "" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPublic",
                table: "Songs");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 879, DateTimeKind.Local).AddTicks(376),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 802, DateTimeKind.Local).AddTicks(3569));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Like_Song",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 911, DateTimeKind.Local).AddTicks(5381),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 838, DateTimeKind.Local).AddTicks(4118));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "User_Cmt_Song",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 913, DateTimeKind.Local).AddTicks(8063),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 840, DateTimeKind.Local).AddTicks(8372));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "Songs",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 897, DateTimeKind.Local).AddTicks(9560),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 824, DateTimeKind.Local).AddTicks(5654));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateCreate",
                table: "PlayList",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 901, DateTimeKind.Local).AddTicks(1952),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 828, DateTimeKind.Local).AddTicks(1790));

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateSendRequest",
                table: "Friends",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2020, 12, 22, 19, 1, 28, 916, DateTimeKind.Local).AddTicks(32),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 12, 27, 9, 58, 32, 842, DateTimeKind.Local).AddTicks(9877));

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                column: "ConcurrencyStamp",
                value: "0069754f-3c54-4134-a302-45ede63cb65b");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                column: "ConcurrencyStamp",
                value: "6666304c-2651-4358-8b7a-df0d7259550a");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                column: "ConcurrencyStamp",
                value: "881053fc-e1b8-4cbe-82fe-e90fff101e8e");

            migrationBuilder.UpdateData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                column: "ConcurrencyStamp",
                value: "a0ae0bb2-439e-4b76-8a56-ed43977fe935");

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 982, DateTimeKind.Local).AddTicks(972));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 982, DateTimeKind.Local).AddTicks(1543));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 982, DateTimeKind.Local).AddTicks(1556));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 982, DateTimeKind.Local).AddTicks(1558));

            migrationBuilder.UpdateData(
                table: "Friends",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateSendRequest",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 982, DateTimeKind.Local).AddTicks(1560));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(4180));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 2,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8122));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 3,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8225));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 4,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8229));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 5,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8232));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 6,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8236));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 7,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8239));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 8,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8241));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 9,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8245));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 10,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8248));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 11,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8251));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 12,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8254));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 13,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8258));

            migrationBuilder.UpdateData(
                table: "PlayList",
                keyColumn: "Id",
                keyValue: 14,
                column: "DateCreate",
                value: new DateTime(2020, 12, 22, 19, 1, 28, 981, DateTimeKind.Local).AddTicks(8261));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9a065fc0-57a7-4291-8abf-9b1d507152f5", "AQAAAAEAACcQAAAAEB8e5dmXPNWpNN6eNxd9Ktx2yXn77xZ7dUjCfN/2ewD46dVRqxu/WDnYvN6LJfK0Uw==", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "567be988-1f9d-424a-a0f5-9e83b4372637", "AQAAAAEAACcQAAAAECD3SnKnLtjiDF5gOBTD5fmUbDrn+0c+asicfN5jTfX2ZpufN095mke0XHtPjwaD4g==", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "8fa4f530-6452-4529-9276-5e9a05fa9607", "AQAAAAEAACcQAAAAED7/BMXCg+1Ta0X7LE4uI4q1skQhRD/7st6x+KQZqWj5mf8UrsNPfrqoQGADMqAucA==", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "fb3d8122-80f0-444d-abe4-5765fdb175dc", "AQAAAAEAACcQAAAAEC+FJ4+cQfvQs2lqanI35C01kaRMWD+WdSVyPiy+rejl4U+1lOFBGSN0BzZyUeRdfQ==", "" });
        }
    }
}

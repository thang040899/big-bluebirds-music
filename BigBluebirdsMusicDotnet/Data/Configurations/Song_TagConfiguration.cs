﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Configurations
{
    public class Song_TagConfiguration : IEntityTypeConfiguration<Song_Tag>
    {
        public void Configure(EntityTypeBuilder<Song_Tag> builder)
        {
            builder.HasKey(t => new { t.SongId, t.TagId });
            builder.ToTable("Song_Tags");
            builder.HasOne(s => s.Song).WithMany(pl => pl.Song_Tags)
                .HasForeignKey(pl => pl.SongId);
            builder.HasOne(pl => pl.Tag).WithMany(s => s.Song_Tags)
              .HasForeignKey(s => s.TagId);
        }
    }
}

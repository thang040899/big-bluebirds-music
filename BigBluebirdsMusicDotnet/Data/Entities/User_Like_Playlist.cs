﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class User_Like_Playlist
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }

        public int PlaylistId { get; set; }
        public PlayList PlayList { get; set; }
        public DateTime DateCreate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Friend
    {
        public int Id { get; set; }
        public Guid SenderId { get; set; }
        public User Sender { get; set; }
        public Guid ReceiverId { get; set; }
        public User Receiver { get; set; }
        public int Status { get; set; }
        public DateTime DateSendRequest { get; set; }
        public DateTime DateUpdateRequest { get; set; }


    }
}

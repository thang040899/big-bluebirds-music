﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class SongType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Song_SongType> Song_SongTypes { get; set; }
    }
}

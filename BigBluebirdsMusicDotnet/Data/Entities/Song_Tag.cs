﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Song_Tag
    {
        public int SongId { get; set; }
        public Song Song { get; set; }

        public int TagId { get; set; }
        public Tag Tag { get; set; }
    }
}

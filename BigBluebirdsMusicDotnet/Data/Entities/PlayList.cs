﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class PlayList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Thumbnail { get; set; }
        public DateTime DateCreate { get; set; }
        public int TotalSong { get; set; }
        public int TotalListen { get; set; }
        public int TotalLike { get; set; }
        public int TotalCmt { get; set; }
        public string PlaylistType { get; set; }

        public List<Song_PlayList> Song_PlayLists { get; set; }
        //public List<FileImage> FileImages { get; set; }

        public List<User_Like_Playlist> ListUserLike { get; set; }
        public List<User_Cmt_Playlist> ListUserCmt { get; set; }
        //public User Owner { get; set; }
        public Guid OwnerId { get; set; }
    }
}

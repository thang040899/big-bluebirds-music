﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.System.User.UserRequest
{
    public class UserCmtRequest
    {
        //public Guid UserId { get; set; }
        public int ObjId { get; set; }
        public string Content { get; set; }
    }
}

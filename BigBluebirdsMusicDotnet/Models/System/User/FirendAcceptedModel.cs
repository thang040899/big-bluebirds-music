﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.System.User
{
    public class FirendAcceptedModel
    {
        public Guid FriendId { get; set; }
        public string FriendName { get; set; }
        public string Status { get; set; }
        public DateTime DateSendRequest { get; set; }
        public DateTime DateAcceptedRequest { get; set; }
    }
}

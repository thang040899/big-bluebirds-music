﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Page
{
    public class PageResult<T>
    {
        public int TotalRecored { get; set; }
        public List<T> Items { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Result
{
    public class CreatedResponse
    {
        public string Msg { get; set; }
        public string ObjectIdCreated { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Catalog.Owner
{
    public class SingerClone
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
    }
}

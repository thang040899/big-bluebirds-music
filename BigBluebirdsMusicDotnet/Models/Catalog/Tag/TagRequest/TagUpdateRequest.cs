﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Catalog.Tag.TagRequest
{
    public class TagUpdateRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}

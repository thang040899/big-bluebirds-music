﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Catalog.History;
using Application.Catalog.PlayList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models.Catalog.History.Request;
using Models.Catalog.PlayList.PlayListRequest;
using Models.Page;
using Models.Result;
using Models.System.User.UserRequest;

namespace BackendAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayListsController : ControllerBase
    {
        private readonly IManagePlayListService _managePlayListService;
        private readonly IManageHistoryService _manageHistoryService;
        public PlayListsController(IManagePlayListService manageSongService, IManageHistoryService manageHistoryService)
        {
            _managePlayListService = manageSongService;
            _manageHistoryService = manageHistoryService;
        }
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] PagingRequest rq)
        {
            var rs = await _managePlayListService.GetAllPlayList(rq);
            if (rs != null)
                return Ok(new OkResApi(){Msg="Get playlist OK",Data=rs});
            else
                return NotFound(new NotfoundResApi() { Msg =""});
        }

        [HttpGet("ById")]
        public async Task<IActionResult> GetPlaylistById([FromQuery] int Id)
        {
            var playlist = await _managePlayListService.GetPlayListById(Id);
            if (playlist != null)
            {
                await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                {
                    ActionType = Models.Enum.ActionType.SEARCH,
                    ObjectType = Models.Enum.ObjectType.PLAYLIST,
                    ObjectId = Id.ToString()
                });
                return Ok(new OkResApi() { Msg = "Get playlist OK", Data = playlist });
            }

            else
                return NotFound(new NotfoundResApi() { Msg = "Cant not find playlist by Id: " + Id });
        }

        [HttpGet("ByName")]
        public async Task<IActionResult> GetPlaylistByName([FromQuery] string Name, [FromQuery] PagingRequest pg)
        {
            var rs = await _managePlayListService.GetPlayListByName(Name, pg);
            if (rs != null)
            {
                await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                {
                    ActionType = Models.Enum.ActionType.SEARCH,
                    ObjectType = Models.Enum.ObjectType.PLAYLIST,
                    ObjectName = Name
                });
                return Ok(new OkResApi() { Msg = "Get playlist OK", Data = rs });
            }
            else
                return NotFound("Can't find playlist with Name: " + Name);
        }

        [HttpGet("PlaylistPrivate")]
        [Authorize]
        public async Task<IActionResult> GetPlaylistPrivate([FromQuery] PagingRequest pg)
        {
            var rs = await _managePlayListService.GetPlayListPrivate( pg);
            if (rs != null)
            {
                return Ok(new OkResApi() { Msg = "Get playlist private OK", Data = rs });
            }
            else
                return BadRequest(new BadRequestResApi() { Msg = "You haven't any playlist" });
        }
        [HttpPost]
        public async Task<IActionResult> Create([FromForm] PlayListCreateRequest rq)
        {
            if (!User.Identity.IsAuthenticated)
                return Unauthorized(new UnauthorResApi());

            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var playlistId = await _managePlayListService.PlayListCreate( rq);
            if (playlistId == 0)
                return BadRequest(new BadRequestResApi() { Msg = "Create playlist FAILED" });
            return Ok(new OkCUDApi() { Msg = "Create playlist OK", ObjectId = playlistId.ToString()});
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery] int Id)
        {
            if (!User.Identity.IsAuthenticated)
                return Unauthorized(new UnauthorResApi());
            var affectedResult = await _managePlayListService.PlayListDelete( Id);
            switch (affectedResult)
            {
                case 0:
                    return NotFound(new NotfoundResApi() { Msg = "Cant not find playlist by Id: " + Id });
                case -403:
                    return StatusCode(403,new ForbirdResApi());
                default:
                    return Ok(new OkCUDApi() { Msg = "Delete playlist OK", ObjectId = affectedResult.ToString() });
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromQuery] int Id, [FromForm] PlayListUpdateRequest rq)
        {
            if (!User.Identity.IsAuthenticated)
                return Unauthorized(new UnauthorResApi());
            var affectedResult = await _managePlayListService.PlayListUpdate( Id,rq);
            switch (affectedResult)
            {
                case 0:
                    return NotFound(new NotfoundResApi() { Msg = "Cant not find playlist by Id: " + Id });
                case -403:
                    return StatusCode(403,new ForbirdResApi());
                default:
                    return Ok(new OkCUDApi() { Msg = "Update playlist OK", ObjectId = affectedResult.ToString() });
            }            
        }

        [HttpPost("Push")]
        public async Task<IActionResult> PushSongToPlayList([FromQuery] int IdPlayList, [FromQuery] int IdSong)
        {
            var rs = await _managePlayListService.PushSongToPlayList(IdPlayList, IdSong);
            switch (rs)
            {
                case -403:
                    return StatusCode(403,new ForbirdResApi());
                case -1:
                    return BadRequest(new BadRequestResApi() { Msg = "Song has been added to this playlist" });
                case 0:
                    return NotFound("Song or Playlist not exist");
                default:
                    return Ok(new OkCUDApi() { Msg = "Push song to playlist OK", ObjectId = IdPlayList.ToString() });

            }
        }

        [HttpDelete("Remove")]
        public async Task<IActionResult> RemoveSongFromPlayist([FromQuery] int IdPlayList, [FromQuery] int IdSong)
        {
            var rs = await _managePlayListService.RemoveSongFromPlayList(IdPlayList, IdSong);
            switch (rs)
            {
                case -403:
                    return StatusCode(403,new ForbirdResApi());
                case -1:
                    return BadRequest(new BadRequestResApi() { Msg = "Song not exist in this playlist" });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Song or Playlist not exist" });
                default:
                    return Ok(new OkCUDApi() { Msg = "Remove song from playlist OK", ObjectId = IdPlayList.ToString() });

            }
        }

        [HttpGet("ListSong")]
        public async Task<IActionResult> GetSongs([FromQuery] int Id, [FromQuery] PagingRequest pg)
        {
            var affectedResult = await _managePlayListService.GetAllSongFromPlayList(Id, pg);
            if (affectedResult == null)
                return BadRequest(new BadRequestResApi() { Msg = "Get song from play list FAILED!" });
            return Ok(new OkResApi() { Msg ="Get song from playlist OK", Data = affectedResult });
        }

        [HttpGet("Comment")]
        public async Task<IActionResult> GetAllCmt([FromQuery] int Id, [FromQuery] PagingRequest pg)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _managePlayListService.GetAllComment(Id, pg);
            if (rs == null)
                return BadRequest(new BadRequestResApi() { Msg = "Cant not find playlist by Id: " + Id });
            return Ok(new OkResApi() { Msg = "Get comment playlist OK", Data = rs });

        }

        #region LikeAPI
        [HttpPost("Like")]
        public async Task<IActionResult> LikePlaylist([FromQuery] int Id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _managePlayListService.LikePlaylist( Id);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Playlist or User not exist" });
                case -1:
                    return Ok(new OkCUDApi() { Msg = "Playlist has been liked by this user", ObjectId = Id.ToString()});
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Like Playlist FAILED" });
                default:
                    await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                    {
                        ActionType = Models.Enum.ActionType.LIKE,
                        ObjectType = Models.Enum.ObjectType.PLAYLIST,
                        ObjectId = Id.ToString()
                    });
                    return Ok(new OkCUDApi() { Msg = "Like Playlist successful", ObjectId =Id.ToString() });
            }
        }

        [HttpGet("CheckLike")]
        public async Task<IActionResult> CheckLikePlaylist([FromQuery] int Id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _managePlayListService.CheckLikePlaylist(Id);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Playlist or User not exist" });              
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Like Playlist FAILED" });
                case 1:
                    return Ok(new OkCUDApi() { Msg = "Liked",Data=true, ObjectId = Id.ToString() });
                default:
                    return Ok(new OkCUDApi() { Msg = "Not Like",Data=false, ObjectId = Id.ToString() });
            }
        }

        [HttpPost("Unlike")]
        public async Task<IActionResult> UnLikePlaylist([FromQuery] int Id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _managePlayListService.UnLikePlaylist( Id);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Playlist or User not exist" });
                case -1:
                    return Ok(new OkCUDApi() { Msg = "Playlist has been not liked by this user", ObjectId = Id.ToString() });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Like Playlist FAILED" });
                default:
                    await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                    {
                        ActionType = Models.Enum.ActionType.LIKE,
                        ObjectType = Models.Enum.ObjectType.PLAYLIST,
                        ObjectId = Id.ToString()
                    });
                    return Ok(new OkCUDApi() { Msg = "UnLike Playlist successful", ObjectId = Id.ToString() });
            }
        }
        #endregion
        #region CommentAPI
        [HttpPost("Comment")]
        public async Task<IActionResult> CmtPlaylist([FromBody] UserCmtRequest rq)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _managePlayListService.CmtPlaylist( rq);
            switch (rs)
            {
                //case -1:
                //    return BadRequest("Playlist has been cmt by this user");
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Playlist or User not exist" });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Comment Playlist FAILED" });
                default:
                    await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                    {
                        ActionType = Models.Enum.ActionType.SEARCH,
                        ObjectType = Models.Enum.ObjectType.PLAYLIST,
                        ObjectId = rq.ObjId.ToString(),
                    });
                    return Ok(new OkCUDApi() { Msg = "Comment Playlist successful", ObjectId = rq.ObjId.ToString()});
            }
        }
        #endregion
    }
}

﻿using Application.AuthorData;
using Application.Catalog.History;
using Application.Catalog.Song;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models.Catalog.History.Request;
using Models.Catalog.Song.SongRequest;
using Models.Page;
using Models.Result;
using Models.System.User.UserRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BackendAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class SongsController : ControllerBase
    {
        private readonly IManageSongService _manageSongService;
        //private readonly IAuthorizationService _authorizationService;
        private readonly IManageHistoryService _manageHistoryService;
        public SongsController(IManageSongService manageSongService, IManageHistoryService manageHistoryService)
        {
            _manageSongService = manageSongService;
            //_authorizationService = authorizationService;
            _manageHistoryService = manageHistoryService;
        }

        [HttpGet("ById")]
        public async Task<IActionResult> GetSongById([FromQuery] int Id)
        {
            var song = await _manageSongService.GetSongById(Id);
            if (song != null)
            {
                await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                {
                    ActionType = Models.Enum.ActionType.SEARCH,
                    ObjectType = Models.Enum.ObjectType.SONG,
                    ObjectId = Id.ToString()
                });
                return Ok(new OkResApi() { Msg = "Get song ok!", Data = song });
            }
            else
                return NotFound(new NotfoundResApi() { Msg = "Can't find song with Id: " + Id });
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] PagingRequest pg)
        {
            var rs = await _manageSongService.GetAllSong(pg);
            if (rs != null)
                return Ok(new OkResApi { Msg = "Get all song OK", Data = rs });
            else
                return NotFound(new NotfoundResApi() { Msg = "Get song FAILED" });
        }

        [HttpGet("ByName")]
        public async Task<IActionResult> GetSongByName([FromQuery] string Name, [FromQuery] PagingRequest pg)
        {
            var rs = await _manageSongService.GetSongByName(Name, pg);

            if (rs != null)
            {
                await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                {
                    ActionType = Models.Enum.ActionType.SEARCH,
                    ObjectType = Models.Enum.ObjectType.SONG,
                    ObjectName = Name
                });
                return Ok(new OkResApi() { Msg = "Get song by name OK", Data = rs });
            }

            else
                return NotFound(new NotfoundResApi() { Msg = "Get song by name FAILED" });
        }

        [HttpGet("MySong")]
        public async Task<IActionResult> GetMySong([FromQuery] PagingRequest pg)
        {
            var rs = await _manageSongService.GetPrivateSong(pg);

            if (rs != null)
            {
                return Ok(new OkResApi() { Msg = "Get song OK", Data = rs });
            }
            else
                return NotFound(new NotfoundResApi() { Msg = "Get song FAILED" });
        }

        [HttpPost]
        public async Task<IActionResult> CreateSong([FromForm] SongCreateRequest rq)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageSongService.SongCreate(rq);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Create song FAILED!" });
                default:
                    return Ok(new OkCUDApi() { Msg = "Create song OK", ObjectId = rs.ToString() });

            }
        }
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery] int Id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageSongService.SongDelete( Id);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -403:
                    return StatusCode(403, new ForbirdResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Can't find song with Id: " + Id });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Delete song FAILED!" });
                default:
                    return Ok(new OkCUDApi() { Msg = "Delete song OK!", ObjectId = rs.ToString(), });
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromQuery] int Id, [FromForm] SongUpdateRequest rq)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageSongService.SongUpdate( Id, rq);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -403:
                    return StatusCode(403, new ForbirdResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Can't find song with Id: " + Id });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Update song FAILED" });
                default:
                    return Ok(new OkCUDApi() { Msg = "Update song OK", ObjectId = rs.ToString() });
            }
        }

        [HttpGet("ByListId")]
        public async Task<IActionResult> GetSongByListId([FromQuery] List<int> ListId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageSongService.GetSongByListId(ListId);
            if (rs == null)
                return BadRequest(new BadRequestResApi() { Msg = "Get song FAILED" });
            return Ok(new OkResApi() { Msg = "Get song OK", Data = rs });
        }

        [HttpGet("Comment")]
        public async Task<IActionResult> GetAllCmt([FromQuery] int Id, [FromQuery] PagingRequest pg)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageSongService.GetAllComment(Id, pg);
            if (rs == null)
                return NotFound(new NotfoundResApi() { Msg = "Cant not find song by Id: " + Id });
            return Ok(new OkResApi() { Msg = "Get comment song OK", Data = rs });

        }

        [HttpPost("Like")]
        public async Task<IActionResult> LikeSong([FromQuery] int Id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageSongService.LikeSong( Id);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Song or User not exist" });
                case -1:
                    return Ok(new OkCUDApi() { Msg = "Song has been liked by this user", ObjectId = Id.ToString() });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Like song FAILED" });
                default:
                    await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                    {
                        ActionType = Models.Enum.ActionType.LIKE,
                        ObjectType = Models.Enum.ObjectType.SONG,
                        ObjectId = Id.ToString()
                    });
                    return Ok(new OkCUDApi() { Msg = "Like song OK", ObjectId = Id.ToString() });
            }
        }

        [HttpGet("CheckLike")]
        public async Task<IActionResult> CheckLikeSong([FromQuery] int Id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageSongService.CheckLikeSong(Id);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Song or User not exist" });                
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "CheckLike song FAILED" });
                case 1:
                    return Ok(new OkCUDApi() { Msg = "Liked",Data=true, ObjectId = Id.ToString() });
                default:
                    return Ok(new OkCUDApi() { Msg = "Not Like",Data=false, ObjectId = Id.ToString() });
            }
        }

        [HttpPost("UnLike")]
        public async Task<IActionResult> UnLikeSong([FromQuery] int Id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageSongService.UnLikeSong( Id);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Song or User not exist" });
                case -1:
                    return Ok(new OkCUDApi() { Msg = "Song has been not liked by this user", ObjectId = Id.ToString() });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "UnLike song FAILED" });
                default:
                    await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                    {
                        ActionType = Models.Enum.ActionType.LIKE,
                        ObjectType = Models.Enum.ObjectType.SONG,
                        ObjectId = Id.ToString()
                    });
                    return Ok(new OkCUDApi() { Msg = "UnLike song OK", ObjectId = Id.ToString() });
            }
        }
        [HttpPost("Comment")]
        public async Task<IActionResult> CmtSong([FromBody] UserCmtRequest rq)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageSongService.CmtSong( rq);
            switch (rs)
            {
                //case -1:
                //    return BadRequest("Song has been cmt by this user");
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Song or User not exist" });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Comment Song FAILED" });
                default:
                    await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                    {
                        ActionType = Models.Enum.ActionType.COMMENT,
                        ObjectType = Models.Enum.ObjectType.SONG,
                        ObjectId = rq.ObjId.ToString()
                    });
                    return Ok(new OkCUDApi() { Msg = "Comment Song OK", ObjectId = rq.ObjId.ToString() });
            }
        }
        [HttpPatch("Listen")]
        public async Task<IActionResult> Listen([FromQuery] int Id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageSongService.Listen(Id);
            switch (rs)
            {
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Song not exist" });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Listen song FAILED" });
                default:
                    return Ok(new OkCUDApi() { Msg = "Listen song OK", ObjectId = Id.ToString() });
            }

        }
        [HttpPatch("Download")]
        public async Task<IActionResult> Download([FromQuery] int Id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            if (!User.Identity.IsAuthenticated)
                return Unauthorized(new UnauthorResApi());
            var rs = await _manageSongService.Download(Id);
            switch (rs)
            {
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Song not exist" });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Download song FAILED" });
                default:
                    await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                    {
                        ActionType = Models.Enum.ActionType.DOWNLOAD,
                        ObjectType = Models.Enum.ObjectType.SONG,
                        ObjectId = Id.ToString()
                    });
                    return Ok(new OkCUDApi() { Msg = "Dowload song OK", ObjectId = Id.ToString() });
            }

        }

        [HttpPost("SongClone")]
        public async Task<IActionResult> SongClone([FromBody] List<SongCloneRequest> rq,[FromQuery]int PlaylistId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageSongService.SongClone(rq,PlaylistId);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Clone song FAILED!" });
                default:
                    return Ok(new OkCUDApi() { Msg = "Clone song OK, number clone: "+ rs.ToString(), ObjectId = rs.ToString() });
            }
        }

        [HttpGet("Top10")]
        public async Task<IActionResult> GetTop10([FromQuery] string Type)
        {
            var rs = await _manageSongService.GetTopSong(Type);
            if (rs != null)
                return Ok(new OkResApi { Msg = "Get top song by "+ Type+ " OK", Data = rs });
            else
                return NotFound(new NotfoundResApi() { Msg = "Get top song FAILED" });
        }
    }
}

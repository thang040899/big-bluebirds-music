﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.System.Dashboard;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models.Result;

namespace BackendAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardsController : ControllerBase
    {
        private readonly IManageDbService _manageDbService;
        public DashboardsController(IManageDbService manageDbService)
        {
            _manageDbService = manageDbService;
        }

        [HttpGet("ChartSong")]
        public async Task<IActionResult> GetChartSong([FromQuery] string Type)
        {
            var songchart = await _manageDbService.GetNumberSong(Type);
            return Ok(new OkResApi() { Msg = "Dashboer numbersong", Data = songchart });
        }

        [HttpGet("ChartPlaylist")]
        public async Task<IActionResult> GetChartPlaylist([FromQuery] string Type)
        {
            var playlistchart = await _manageDbService.GetNumberPlaylist(Type);
            return Ok(new OkResApi() { Msg = "Dashboard number playlist", Data = playlistchart });
        }

        [HttpGet("TotalPlaylist")]
        public async Task<IActionResult> GetTotalPlaylist()
        {
            var playlistchart = await _manageDbService.GetTotalPlaylist();
            return Ok(new OkResApi() { Msg = "Dashboard total playlist", Data = playlistchart });
        }

        [HttpGet("ChartAll")]
        public async Task<IActionResult> GetAll([FromQuery] string type)
        {
            var all = await _manageDbService.GetAll(type);
            return Ok(new OkResApi() { Msg = "Dashboard all", Data = all });
        }

        [HttpGet("ChartAge")]
        public async Task<IActionResult> GetAge([FromQuery] string type)
        {
            var all = await _manageDbService.GetAge(type);
            return Ok(new OkResApi() { Msg = "Dashboard Age", Data = all });
        }

        [HttpGet("ChartCreateUser")]
        public async Task<IActionResult> GetCreateUser([FromQuery] string type)
        {
            var all = await _manageDbService.GetCreateUser(type);
            return Ok(new OkResApi() { Msg = "Dashboard Create User", Data = all });
        }
    }
}

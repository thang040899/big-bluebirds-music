﻿using Application.Catalog.History;
using Application.System.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.Catalog.History.Request;
using Models.Page;
using Models.Result;
using Models.System.User.UserRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BackendAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IManageUserService _manageUserService;
        private readonly IManageHistoryService _manageHistoryService;

        public UsersController(IManageUserService manageUserService,IManageHistoryService manageHistoryService)
        {
            _manageUserService = manageUserService;
            _manageHistoryService = manageHistoryService;
        }
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] UserLoginRequest rq)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var resultToken = await _manageUserService.Login(rq);
            if (resultToken == null)
                return BadRequest(new BadRequestResApi() { Msg = "Username or password incorect"});
            await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
            {
                ActionType = Models.Enum.ActionType.LOGIN,
                ObjectType = Models.Enum.ObjectType.SONG,
                //ObjectId = User.FindFirst(ClaimTypes.NameIdentifier).Value
                ObjectName = rq.User
            });
            return Ok(new OkResApi() { Msg ="Login OK", Data =resultToken});
        }
        [HttpPost("LoginWithFacebook")]
        public async Task<IActionResult> LoginFB([FromQuery] string Token)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var resultToken = await _manageUserService.LoginWithFacebook(Token);
            await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
            {
                ActionType = Models.Enum.ActionType.LOGIN,
                ObjectType = Models.Enum.ObjectType.SONG,
                //ObjectId = User.FindFirst(ClaimTypes.NameIdentifier).Value
                ObjectName = ""
            });
            return Ok(new OkResApi() { Msg = "Login OK", Data = resultToken });
        }
        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromForm] UserRegisterRequest rq)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var result = await _manageUserService.Register(rq);
            if (result == null)
                return BadRequest(new BadRequestResApi() { Msg = "Register FAILED" });
            return Ok(new OkResApi() { Msg = "Register OK", Data = result });
        }
        [HttpGet]
        public async Task<IActionResult> GetAllUser([FromQuery]PagingRequest pg)
        {
            var result = await _manageUserService.GetAllUser(pg);
            return Ok(new OkResApi() { Msg ="Get user OK", Data =result});
        }
        [HttpGet("ByUsername")]      
        public async Task<IActionResult> GetByUsername([FromQuery] string UserName)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var result = await _manageUserService.GetByUsername(UserName);
            await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
            {
                ActionType = Models.Enum.ActionType.LOGIN,
                ObjectType = Models.Enum.ObjectType.USER,
                ObjectName = UserName
            });
            return Ok(new OkResApi() { Msg = "Get user OK", Data = result });
        }       
        [HttpGet("ById")]
        public async Task<IActionResult> GetById([FromQuery] Guid id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var result = await _manageUserService.GetById(id);
            if (result != null)
                return Ok(new OkResApi() { Msg = "Get user OK", Data = result });
            else
                return NotFound(new NotfoundResApi() { Msg ="Can't not find user with Id: "+id.ToString()});
        }       
        [HttpGet("ByPhone")]
        public async Task<IActionResult> GetByPhone([FromQuery] string ListPhone,[FromQuery] PagingRequest pg)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var result = await _manageUserService.GetUserByPhone(ListPhone,pg);
            return Ok(new OkResApi() { Msg = "Get user OK", Data = result });
        }
        [HttpPut]
        public async Task<IActionResult> Update([FromQuery] Guid Id,[FromForm] UserUpdateRequest rq)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageUserService.Update(Id,rq);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -403:
                    return StatusCode(403,new ForbirdResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Can't find user with Id: " + Id.ToString()});
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Update user FAILED!" });
                default:
                    return Ok(new OkCUDApi() { Msg = "Update user OK!", ObjectId = Id.ToString()});
            }
        }
        [HttpDelete]
        [Authorize(Roles ="admin")]
        public async Task<IActionResult> Delete([FromQuery] Guid Id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageUserService.Delete(Id);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -403:
                    return StatusCode(403,new ForbirdResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Can't find user with Id: " + Id.ToString() });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Delete user FAILED!" });
                default:
                    return Ok(new OkCUDApi() { Msg = "Delete user OK!", ObjectId = Id.ToString() });
            }
        }
        [HttpPut("SetRole")]
        public async Task<IActionResult> SetRole([FromQuery] Guid Id, [FromQuery] string RoleName)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageUserService.SetRole(Id, RoleName);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -403:
                    return StatusCode(403, new ForbirdResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Can't find user with Id: " + Id.ToString() });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Set Role user FAILED!" });
                default:
                    return Ok(new OkCUDApi() { Msg = "Set Role user OK!", ObjectId = Id.ToString() });
            }
        }
        [HttpGet("CurrentUser")]
        public async Task<IActionResult> GetCurrentUser()
        {
            if (!User.Identity.IsAuthenticated)
                return Unauthorized(new UnauthorResApi());
            var rs =  await _manageUserService.GetCurrentUser();
            if (rs != null)
                return Ok(new OkResApi() { Msg ="Get current OK", Data =rs});
            else
                return NotFound(new NotfoundResApi() { Msg = "Not found current user" });
        }
        #region Friend
        [HttpGet("AllStatusFriend")]
        public async  Task<IActionResult> GetAllStatusFriend()
        {
            //if (!ModelState.IsValid)
            //    return BadRequest(ModelState);
            if (!User.Identity.IsAuthenticated)
                return Unauthorized(new UnauthorResApi());
            var rs = await _manageUserService.GetAllStatusFiend();
            if (rs == null)
                return NotFound(new NotfoundResApi() { Msg = "Not found user" });
            else
                return Ok(new OkResApi() { Msg ="Get all status friend OK", Data = rs});
        }
        [HttpGet("AllFriend")]
        public async Task<IActionResult> GetAllFriend()
        {
            //if (!ModelState.IsValid)
            //    return BadRequest(ModelState);
            if (!User.Identity.IsAuthenticated)
                return Unauthorized(new UnauthorResApi());
            var rs = await _manageUserService.GetAllFiend();
            if (rs == null)
                return NotFound(new NotfoundResApi() { Msg = "Not found user" });
            else
                return Ok(new OkResApi() { Msg = "Get all friend OK", Data = rs });
        }
        [HttpPost("SendFriendRequest")]
        public async Task<IActionResult> SendFriendRequest([FromQuery] Guid ReceiverId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageUserService.SendFriendRequest(ReceiverId);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Sender or Receiver not exist" });
                case -2:
                    return BadRequest(new BadRequestResApi() { Msg = "Cant send friend request yourselft" });
                case -1:
                    return BadRequest(new BadRequestResApi() { Msg = "Request is accepted before" });
                case 0:
                    await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                    {
                        ActionType = Models.Enum.ActionType.SENDFRIENDREQUEST,
                        ObjectType = Models.Enum.ObjectType.FRIEND,
                        ObjectId = ReceiverId.ToString()
                    });
                    return Ok(new OkResApi() { Msg ="You have send friend request to this user"});
                case 1:
                    await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                    {
                        ActionType = Models.Enum.ActionType.ACCEPTFRIENDREQUEST,
                        ObjectType = Models.Enum.ObjectType.FRIEND,
                        ObjectId = ReceiverId.ToString()
                    });
                    return Ok(new OkResApi() { Msg = "Make friend successful! because this user have send request before" });
                default:
                    await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                    {
                        ActionType = Models.Enum.ActionType.SENDFRIENDREQUEST,
                        ObjectType = Models.Enum.ObjectType.FRIEND,
                        ObjectId = ReceiverId.ToString()
                    });
                    return Ok(new OkCUDApi() { Msg = "Send request successful!", ObjectId = ReceiverId.ToString()});
            }
        }
        [HttpPost("AcceptFriendRequest")]
        public async Task<IActionResult> AcceptFriendRequest([FromQuery] Guid SenderId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageUserService.AcceptFriendRequest(SenderId);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Friend request is not exist" });
                case -1:
                    return BadRequest(new BadRequestResApi() { Msg = "Friend request not exist" });
                case 1:
                    await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                    {
                        ActionType = Models.Enum.ActionType.ACCEPTFRIENDREQUEST,
                        ObjectType = Models.Enum.ObjectType.FRIEND,
                        ObjectId = SenderId.ToString()
                    });
                    return Ok(new OkCUDApi() { Msg = "Deny friend request OK", ObjectId = SenderId.ToString() });
                default:                  
                    return BadRequest(new BadRequestResApi() { Msg = "Accept friend request FAILED" });
            }
        }
        [HttpPost("DenyFriendRequest")]
        public async Task<IActionResult> DenyFriendRequest([FromQuery] Guid SenderId)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageUserService.AcceptFriendRequest(SenderId);
            switch (rs)
            {
                case -401:
                    return Unauthorized(new UnauthorResApi());
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Friend request is not exist" });
                case 1:
                    await _manageHistoryService.RecordHistory( new CreateHistoryRequest()
                    {
                        ActionType = Models.Enum.ActionType.DENYFRIENDREQUEST,
                        ObjectType = Models.Enum.ObjectType.FRIEND,
                        ObjectId = SenderId.ToString()
                    });
                    return Ok(new OkCUDApi() { Msg = "Deny friend request OK", ObjectId = SenderId.ToString() });
                default:
                    return BadRequest(new BadRequestResApi() { Msg = "Deny friend request FAILED" });
            }
        }
        #endregion

    }

}

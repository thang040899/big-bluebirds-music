﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Catalog.Tag;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models.Catalog.Tag.TagRequest;
using Models.Page;
using Models.Result;

namespace BackendAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TagController : ControllerBase
    {
        private readonly IManageTagService _manageTagService;
        public TagController(IManageTagService manageTagService)
        {
            _manageTagService = manageTagService;
        }

        [HttpGet("ById")]
        public async Task<IActionResult> GetTagById([FromQuery] int Id)
        {
            var Tag = await _manageTagService.GetTagById(Id);
            if (Tag != null)
                return Ok(new OkResApi() { Msg = "Get Tag OK", Data = Tag });
            else
                return NotFound(new NotfoundResApi() { Msg = "Cant not find Tag by Id: " + Id });
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] PagingRequest pg)
        {
            var rs = await _manageTagService.GetAllTag(pg);
            if (rs != null)
                return Ok(new OkResApi() { Msg = "Get Tag OK", Data = rs });
            else
                return NotFound(new NotfoundResApi() { Msg = "Cant not find Tag" });
        }

        [HttpGet("ByName")]
        public async Task<IActionResult> GetByName([FromQuery] string Name, [FromQuery] PagingRequest pg)
        {
            var rs = await _manageTagService.GetTagByName(Name, pg);
            if (rs != null)
                return Ok(new OkResApi() { Msg = "Get Tag OK", Data = rs });
            else
                return NotFound(new NotfoundResApi() { Msg = "Cant not find Tag by name: " + Name });
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Create([FromForm] TagCreateRequest rq)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageTagService.TagCreate(rq);
            switch(rs)
            {
                case -1:
                    return BadRequest(new BadRequestResApi() { Msg = "Name tag have exist" });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Create Tag FAILED" });
                default:
                    return Ok(new OkCUDApi() { Msg = "Create tag OK", ObjectId = rs.ToString() });
            }
        }

        [HttpDelete]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete([FromQuery] int Id)
        {

            var rs = await _manageTagService.TagDelete(Id);
            switch (rs)
            {
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Not found Tag" });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Delete Tag FAILED" });
                default:
                    return Ok(new OkCUDApi() { Msg = "Delete tag OK", ObjectId = rs.ToString() });
            }
        }

        [HttpPut]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Update([FromQuery] int Id, [FromBody] TagUpdateRequest rq)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageTagService.TagUpdate(Id, rq);
            switch (rs)
            {
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Not found Tag " });
                case -1:
                    return BadRequest(new BadRequestResApi() { Msg = "Name tag have exist" });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Update Tag FAILED" });
                default:
                    return Ok(new OkCUDApi() { Msg = "Update tag OK", ObjectId = rs.ToString() });
            }
        }

        [HttpGet("ListSong")]
        public async Task<IActionResult> GetListSong([FromQuery] int Id, [FromQuery] PagingRequest pg)
        {
            var affectedResult = await _manageTagService.GetSongFromTag(Id, pg);
            if (affectedResult == null)
                return BadRequest(new BadRequestResApi() { Msg = "Get list song from Tag FAILED" });
            return Ok(new OkResApi() { Msg = "Get list song from Tag FAILED OK", Data = affectedResult });
        }
    }
}

﻿using Application.Catalog.SongType;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.Catalog.SongType.SongTypeRequest;
using Models.Page;
using Models.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SongTypesController : ControllerBase
    {
        private readonly IManageSongTypeService _manageSongTypeService;
        public SongTypesController(IManageSongTypeService manageSongTypeService)
        {
            _manageSongTypeService = manageSongTypeService;
        }

        [HttpGet("ById")]
        public async Task<IActionResult> GetSongById([FromQuery]int Id)
        {
            var SongType = await _manageSongTypeService.GetSongTypeById(Id);
            if (SongType != null)
                return Ok(new OkResApi() { Msg = "Get songtype OK", Data = SongType });
            else
                return NotFound(new NotfoundResApi() { Msg = "Cant not find songtype by Id: " + Id });
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery]PagingRequest pg)
        {
            var rs = await _manageSongTypeService.GetAllSongType(pg);
            if (rs != null)
                return Ok(new OkResApi() { Msg = "Get songtype OK", Data = rs });
            else
                return NotFound(new NotfoundResApi() { Msg = "Cant not find songtype"});
        }

        [HttpGet("ByName")]
        public async Task<IActionResult> GetByName([FromQuery] string Name,[FromQuery]PagingRequest pg)
        {
            var rs = await _manageSongTypeService.GetSongTypeByName(Name,pg);
            if (rs != null)
                return Ok(new OkResApi() { Msg = "Get songtype OK", Data = rs});
            else
                return NotFound(new NotfoundResApi() { Msg = "Cant not find songtype by name: " + Name });
        }

        [HttpPost]
        [Authorize(Roles ="admin")]
        public async Task<IActionResult> Create([FromForm] SongTypeCreateRequest rq)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageSongTypeService.SongTypeCreate(rq);
            switch (rs)
            {
                case -1:
                    return BadRequest(new BadRequestResApi() { Msg = "Name song type have exist" });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Create song type FAILED" });
                default:
                    return Ok(new OkCUDApi() { Msg = "Create song type OK", ObjectId = rs.ToString() });
            }
        }

        [HttpDelete]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete([FromQuery]int Id)
        {

            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageSongTypeService.SongTypeDelete(Id);
            switch (rs)
            {
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Not found song type" });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Create song type FAILED" });
                default:
                    return Ok(new OkCUDApi() { Msg = "Create song type OK", ObjectId = rs.ToString() });
            }
        }

        [HttpPut]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Update([FromQuery]int Id,[FromBody] SongTypeUpdateRequest rq)
        {

            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var rs = await _manageSongTypeService.SongTypeUpdate(Id,rq);
            switch (rs)
            {
                case -404:
                    return NotFound(new NotfoundResApi() { Msg = "Not found song type" });
                case -1:
                    return BadRequest(new BadRequestResApi() { Msg = "Name song type have exist" });
                case 0:
                    return BadRequest(new BadRequestResApi() { Msg = "Create song type FAILED" });
                default:
                    return Ok(new OkCUDApi() { Msg = "Create song type OK", ObjectId = rs.ToString() });
            }
        }

        [HttpGet("ListSong")]
        public async Task<IActionResult> GetListSong([FromQuery]int Id, [FromQuery]PagingRequest pg)
        {
            var affectedResult = await _manageSongTypeService.GetSongFromSongType(Id,pg);
            if (affectedResult == null)
                return BadRequest(new BadRequestResApi() { Msg = "Get list song from songtype FAILED" });
            return Ok(new OkResApi() { Msg = "Get list song from songtype FAILED OK", Data = affectedResult});
        }


    }

}

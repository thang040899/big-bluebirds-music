﻿using Application.Catalog.Song;
using Data.EF;
using Microsoft.EntityFrameworkCore;
using Models.Catalog.Song;
using Models.Catalog.SongType;
using Models.Catalog.SongType.SongTypeRequest;
using Models.Page;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Catalog.SongType
{
    public class ManageSongTypeService : IManageSongTypeService
    {
        private readonly BigBluebirdsDbContext _context;
        private readonly IManageSongService _manageSongService;
        public ManageSongTypeService(BigBluebirdsDbContext context, IManageSongService manageSongService)
        {
            _context = context;
            _manageSongService = manageSongService;
        }

        public async Task<List<SongTypeModel>> GetAllSongType(PagingRequest pg)
        {
            var query = from sp in _context.SongTypes
                        select new { sp };                    
            var data = await query.Select(x => new SongTypeModel()
            {
                Id = x.sp.Id,
                Name = x.sp.Name,
                Description = x.sp.Description,               
            })
                .Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize)
                .ToListAsync();
            return data;
        }
      
        public async Task<SongTypeModel> GetSongTypeById(int Id)
        {

            var sp = await _context.SongTypes.FindAsync(Id);
            if (sp == null)
            {
                return null;
            }
            var data = new SongTypeModel()
            {
                Id = sp.Id,
                Name = sp.Name,
                Description = sp.Description,
            };

            return data;
        }

        public async Task<List<SongTypeModel>> GetSongTypeByName(string Name, PagingRequest pg)
        {
            var query = from sp in _context.SongTypes
                        select new { sp };
            var data = await query.Select(x => new SongTypeModel()
            {
                Id = x.sp.Id,
                Name = x.sp.Name,
                Description = x.sp.Description,
            }).Where(x=>Name.Contains(x.Name)|| x.Name.Contains(Name))
            .Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize)
            .ToListAsync();
            return data;
        }

        public async Task<int> SongTypeCreate(SongTypeCreateRequest rq)
        {
            var checkName = _context.Tags.FirstOrDefault(st => st.Name == rq.Name);
            if (checkName != null)
                return -1;
            var st = new Data.Entities.SongType()
            {
                Name = rq.Name,
                Description = rq.Description
            };
            _context.SongTypes.Add(st);
            await _context.SaveChangesAsync();
            return st.Id;

        }

        public async Task<int> SongTypeDelete(int Id)
        {
            var sp = await _context.SongTypes.FindAsync(Id);
            if (sp == null)
            {
                return -404;
            }
            _context.SongTypes.Remove(sp);
            await _context.SaveChangesAsync();
            return sp.Id;
        }

        public async Task<int> SongTypeUpdate(int Id,SongTypeUpdateRequest rq)
        {
            var sp = await _context.SongTypes.FindAsync(Id);
            if (sp == null)
            {
                return -404;
            }
            var checkName = _context.Tags.FirstOrDefault(st => st.Name == rq.Name);
            if (checkName != null)
                return -1;
            sp.Name = rq.Name;
            sp.Description = rq.Description;
            _context.SongTypes.Update(sp);
            await _context.SaveChangesAsync();
            return sp.Id;
        }


        public async Task<List<SongModel>> GetSongFromSongType(int Id, PagingRequest pg)
        {            
            var ListId = await _context.Song_SongTypes.Where(x => x.SongTypeId == Id).Select(x => x.SongId)
                .Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize)
                .ToListAsync();
            return await _manageSongService.GetSongByListId(ListId);

        }
    }
}

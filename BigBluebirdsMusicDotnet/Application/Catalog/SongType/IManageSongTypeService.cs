﻿using Models.Catalog.Song;
using Models.Catalog.SongType;
using Models.Catalog.SongType.SongTypeRequest;
using Models.Page;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Catalog.SongType
{
    public interface IManageSongTypeService
    {
        Task<SongTypeModel> GetSongTypeById(int Id);      
        Task<List<SongTypeModel>> GetSongTypeByName(string Name,PagingRequest pg);
        Task<List<SongTypeModel>> GetAllSongType(PagingRequest pg);
        Task<int> SongTypeCreate(SongTypeCreateRequest rq);
        Task<int> SongTypeDelete(int Id);
        Task<int> SongTypeUpdate(int Id,SongTypeUpdateRequest rq);
        Task<List<SongModel>> GetSongFromSongType(int Id, PagingRequest pg);
    }
}

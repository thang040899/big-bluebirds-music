﻿using Models.Catalog.History;
using Models.Catalog.History.Request;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Application.Catalog.History
{
    public interface IManageHistoryService
    {
        public Task<bool> RecordHistory(CreateHistoryRequest rq);
        public Task<List<HistoryModel>> GetHistory();
    }
}

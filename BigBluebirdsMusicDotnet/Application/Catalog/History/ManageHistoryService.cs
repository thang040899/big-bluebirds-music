﻿using Data.EF;
using Models.Catalog.History;
using Models.Catalog.History.Request;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Application.Base;

namespace Application.Catalog.History
{
    public class ManageHistoryService : IManageHistoryService
    {
        private readonly BigBluebirdsDbContext _context;
        private readonly IApplicationServiceBase _applicationServiceBase;
        public ManageHistoryService(BigBluebirdsDbContext context, IApplicationServiceBase applicationServiceBase)
        {
            _context = context;
            _applicationServiceBase = applicationServiceBase;
        }
        public async Task<List<HistoryModel>> GetHistory()
        {
            var UserIdFind = new Guid(_applicationServiceBase.GetCurrentUserId());
            var data = await _context.Histories.Where(x=>x.UserId==UserIdFind)
                .Select(x => new HistoryModel()
                {
                    UserId = x.UserId,
                    DateTime = x.DateTime,
                    ActionType = x.ActionType,
                    ObjectType = x.ObjectType,
                    ObjectId = x.ObjectId,
                    ObjectName = x.ObjectName
                }).ToListAsync();
            return data;
        }

        public async Task<bool> RecordHistory(CreateHistoryRequest rq)
        {
            if (!_applicationServiceBase.CheckAuthentication())
            {
                return false;
            }

            else
            {
                Data.Entities.History history = new Data.Entities.History()
                {
                    UserId = new Guid(_applicationServiceBase.GetCurrentUserId()),
                    DateTime = DateTime.Now,
                    ActionType = rq.ActionType.ToString(),
                    ObjectType = rq.ObjectType.ToString(),
                    ObjectId = rq.ObjectId,
                    ObjectName = rq.ObjectName
                };
                await _context.Histories.AddAsync(history);
                await _context.SaveChangesAsync();
                return true;
            }

        }
    }
}

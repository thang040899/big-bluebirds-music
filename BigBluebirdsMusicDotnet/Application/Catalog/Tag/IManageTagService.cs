﻿using Models.Catalog.Song;
using Models.Catalog.Tag;
using Models.Catalog.Tag.TagRequest;
using Models.Page;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Catalog.Tag
{
    public interface IManageTagService
    {
        Task<TagModel> GetTagById(int Id);
        Task<List<TagModel>> GetTagByName(string Name, PagingRequest pg);
        Task<List<TagModel>> GetAllTag(PagingRequest pg);
        Task<int> TagCreate(TagCreateRequest rq);
        Task<int> TagDelete(int Id);
        Task<int> TagUpdate(int Id, TagUpdateRequest rq);
        Task<List<SongModel>> GetSongFromTag(int Id, PagingRequest pg);
    }
}

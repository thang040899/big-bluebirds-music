﻿using Data.EF;
using Models.Catalog.PlayList;
using Models.Catalog.PlayList.PlayListRequest;
using Models.Page;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Data.Entities;
using Models.Catalog.Song;
using Application.Catalog.Song;
using Models.Catalog.Comment;
using System.Security.Claims;
using Models.System.User.UserRequest;
using Application.Common;
using Models.Enum;
using Models.Catalog.Owner;
using Microsoft.Extensions.Configuration;
using Application.Base;

namespace Application.Catalog.PlayList
{
    public class ManagePlayListService : IManagePlayListService
    {

        private readonly BigBluebirdsDbContext _context;
        private readonly IApplicationServiceBase _applicationServiceBase;
        private readonly IManageSongService _manageSongService;
        private readonly IStorageService _storageService;
        private readonly IConfiguration _config;
        public ManagePlayListService(BigBluebirdsDbContext context,
            IApplicationServiceBase applicationServiceBase,
            IManageSongService manageSongService, IStorageService storageService, IConfiguration config)
        {
            _context = context;
            _applicationServiceBase = applicationServiceBase;
            _manageSongService = manageSongService;
            _storageService = storageService;
            _config = config;
        }

        #region CRUD Playlist
        private PlayListModel MapPlaylist(Data.Entities.PlayList pl)
        {
            var data = new PlayListModel()
            {
                Id = pl.Id,
                Name = pl.Name,
                Description = pl.Description,
                DateCreate = pl.DateCreate,
                TotalListen = pl.TotalListen,
                TotalLike = pl.TotalLike,
                TotalCmt = pl.TotalCmt,
                TotalSong = pl.TotalSong,
                PlaylistType = pl.PlaylistType,
                Thumbnail =pl.Thumbnail,
                Owner = _context.Users.Where(z => z.Id == pl.OwnerId).Select(c => new OwnerModel()
                {
                    OwnerId = c.Id,
                    NameOwner = c.FirstName + " " + c.LastName
                }).FirstOrDefault(),
            };

            //var srcImg = _context.FileImages.Where(z => z.IdPlayList == pl.Id && z.Description == "Thumbnail").Select(c => c.Path).FirstOrDefault();
            if (!pl.Thumbnail.Contains("http") && pl.Thumbnail != "")
                data.Thumbnail = _config["File:Image"] + pl.Thumbnail;
            return data;
        }
        private bool CheckAuthorizePlayList(Data.Entities.PlayList pl)
        {
            if (pl.PlaylistType == PlaylistType.SYSTEM.ToString() && _applicationServiceBase.CheckRole("admin"))
                return true;
            if (pl.PlaylistType == PlaylistType.USER.ToString() && _applicationServiceBase.GetCurrentUserId() == pl.OwnerId.ToString())
                return true;
            return false;
        }
        public async Task<List<PlayListModel>> GetAllPlayList(PagingRequest pg)
        {
            var data = await _context.PlayLists.Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize).ToListAsync();
            var rs = data.Select(x => MapPlaylist(x)).ToList();
            return rs;
        }
        public async Task<PlayListModel> GetPlayListById(int Id)
        {
            var pl = await _context.PlayLists.FindAsync(Id);
            if (pl == null)
            {
                return null;
            }
            return MapPlaylist(pl);
        }
        public async Task<List<PlayListModel>> GetPlayListByName(string Name, PagingRequest pg)
        {
            var data = await _context.PlayLists.Where(x => Name.Contains(x.Name) || x.Name.Contains(Name)).Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize).ToListAsync();
            var rs = data.Select(x => MapPlaylist(x)).ToList();
            return rs;
        }
        public async Task<List<PlayListModel>> GetPlayListPrivate( PagingRequest pg)
        {
            var data = await _context.PlayLists.Where(x => x.OwnerId == new Guid(_applicationServiceBase.GetCurrentUserId())).Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize).ToListAsync();
            var rs = data.Select(x => MapPlaylist(x)).ToList();
            return rs;
        }
        public async Task<int> PlayListCreate( PlayListCreateRequest rq)
        {
            var thumbnail="";
            if (rq.Thumbnail!=null)
                thumbnail = await _storageService.SaveFile(rq.Thumbnail, 0);
            if (rq.Description == null)
                rq.Description = "";
            var pl = new Data.Entities.PlayList()
            {
                Name = rq.Name,
                DateCreate = DateTime.Now,
                Description = rq.Description,
                OwnerId = new Guid(_applicationServiceBase.GetCurrentUserId()),
                Thumbnail = thumbnail
            };
            if (_applicationServiceBase.CheckRole("admin"))
                pl.PlaylistType = PlaylistType.SYSTEM.ToString();
            else
                pl.PlaylistType = PlaylistType.USER.ToString();

            //Add Image
            //pl.FileImages = new List<FileImage>();
            //if (rq.Thumbnail != null)
            //{
            //    pl.FileImages.Add(new FileImage()
            //    {
            //        Description = "Thumbnail",
            //        Type = ImageType.SYSTEM.ToString(),
            //        FileSize = rq.Thumbnail.Length,
            //        Path = await _storageService.SaveFile(rq.Thumbnail, 0),
            //    });
            //}
            _context.PlayLists.Add(pl);
            return await _context.SaveChangesAsync();

        }
        public async Task<int> PlayListDelete( int Id)
        {

            var pl = await _context.PlayLists.FindAsync(Id);
            if (pl == null)
            {
                return 0;
            }
            if (!CheckAuthorizePlayList(pl))
                return -403;
            _context.PlayLists.Remove(pl);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> PlayListUpdate( int Id, PlayListUpdateRequest rq)
        {

            var pl = await _context.PlayLists.FindAsync(Id);
            if (pl == null)
            {
                return 0;
            }
            if (!CheckAuthorizePlayList(pl))
                return -403;
            pl.Name = rq.Name;
            if (rq.Description == null)
            {
                rq.Description = "";
            }
            pl.Description = rq.Description;

            if (rq.Thumbnail != null)
            {
                pl.Thumbnail = await _storageService.SaveFile(rq.Thumbnail, 0);
                //var ThumnailUpdate = _context.FileImages.Where(x => x.IdPlayList == Id && x.Description == "Thumbnail").FirstOrDefault();
                //if (ThumnailUpdate == null)
                //{
                //    ThumnailUpdate = new FileImage();
                //    ThumnailUpdate.Description = "Thumbail";
                //    pl.FileImages = new List<FileImage>();
                //    pl.FileImages.Add(ThumnailUpdate);
                //}
                //ThumnailUpdate.Type = ImageType.SYSTEM.ToString();
                //ThumnailUpdate.FileSize = rq.Thumbnail.Length;
                //ThumnailUpdate.Path = await _storageService.SaveFile(rq.Thumbnail, 0);
                //_context.Update(ThumnailUpdate);
            }
            _context.PlayLists.Update(pl);
            return await _context.SaveChangesAsync();
        }
        public async Task<List<SongModel>> GetAllSongFromPlayList(int Id, PagingRequest pg)
        {
            var ListId = await _context.Song_PlayLists.Where(x => x.PlayListId == Id).Select(x => x.SongId)
                .Skip((pg.Index - 1) * pg.PageSize)
                .Take(pg.PageSize).ToListAsync();
            if (ListId.Count == 0)
                return new List<SongModel>();
            var data = await _manageSongService.GetSongByListId(ListId);
            return data;
        }
        #endregion
        public async Task<int> PushSongToPlayList( int IdPlayList, int IdSong)
        {
            var PlayList = await _context.PlayLists.FindAsync(IdPlayList);
            var Song = await _context.Songs.FindAsync(IdSong);
            if (PlayList == null || Song == null)
                return 0;
            var spl = new Song_PlayList()
            {
                SongId = IdSong,
                Song = Song,
                PlayListId = IdPlayList,
                PlayList = PlayList

            };
            if (!CheckAuthorizePlayList(PlayList))
                return -403;
            var rs = await _context.Song_PlayLists.FindAsync(IdSong, IdPlayList);
            if (rs != null)
                return -1;//Bi trung -- bai hat da duoc them vao play list
            _context.Song_PlayLists.Add(spl);
            PlayList.TotalSong++;
            _context.PlayLists.Update(PlayList);
            return await _context.SaveChangesAsync();

        }
        public async Task<int> RemoveSongFromPlayList( int IdPlayList, int IdSong)
        {
            var PlayList = await _context.PlayLists.FindAsync(IdPlayList);
            var Song = await _context.Songs.FindAsync(IdSong);
            if (PlayList == null || Song == null)
                return 0;
            if (!CheckAuthorizePlayList(PlayList))
                return -403;
            var rs = await _context.Song_PlayLists.FindAsync(IdSong, IdPlayList);
            if (rs == null)
                return -1;// -- bai hat chua co trong play list
            _context.Song_PlayLists.Remove(rs);

            PlayList.TotalSong--;
            _context.PlayLists.Update(PlayList);
            return await _context.SaveChangesAsync();

        }
        public async Task<List<CommentModel>> GetAllComment(int PlaylistID, PagingRequest pg)
        {
            return await _context.User_Cmt_Playlists.Where(x => x.PlaylistId == PlaylistID).Select(x => new CommentModel
            {
                UserId = x.UserId,
                FirstName = x.User.FirstName,
                LastName = x.User.LastName,
                Content = x.Content,
                DateTime = x.DateCreate
            }).Skip((pg.Index - 1) * pg.PageSize).Take(pg.PageSize).ToListAsync();

        }

        #region Like Unlike
        public async Task<int> LikePlaylist( int IdPlaylist)
        {
            if (!_applicationServiceBase.CheckAuthentication())
                return -401;
            var Id = _applicationServiceBase.GetCurrentUserId();
            Guid UserId = new Guid();
            if (Id != null)
            {
                try
                {
                    UserId = new Guid(Id);
                }
                catch
                {
                    return 0;
                }
            }
            var UserLike = await _context.Users.FindAsync(UserId);
            var Playlist = await _context.PlayLists.FindAsync(IdPlaylist);
            if (Playlist == null)
                return -404;
            var ulp = new User_Like_Playlist()
            {
                UserId = UserId,
                User = UserLike,
                PlaylistId = IdPlaylist,
                PlayList = Playlist,
                DateCreate = DateTime.Now
            };
            var rs = await _context.User_Like_Playlists.Where(x => x.UserId == UserId && x.PlaylistId == IdPlaylist).ToListAsync();
            if (rs.Count != 0)
                return -1;//Bi trung -- playlist da duoc like
            _context.User_Like_Playlists.Add(ulp);
            Playlist.TotalLike++;
            _context.PlayLists.Update(Playlist);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> CheckLikePlaylist(int IdPlaylist)
        {
            if (!_applicationServiceBase.CheckAuthentication())
                return -401;
            var Id = _applicationServiceBase.GetCurrentUserId();
            Guid UserId = new Guid();
            if (Id != null)
            {
                try
                {
                    UserId = new Guid(Id);
                }
                catch
                {
                    return 0;
                }
            }
            var UserLike = await _context.Users.FindAsync(UserId);
            var Playlist = await _context.PlayLists.FindAsync(IdPlaylist);
            if (Playlist == null)
                return -404;
            var rs = await _context.User_Like_Songs.FindAsync(UserId, IdPlaylist);
            if (rs!=null)
                return 1;
            else
                return 2;

        }
        public async Task<int> UnLikePlaylist( int IdPlaylist)
        {
            if (!_applicationServiceBase.CheckAuthentication())
                return -401;
            var Id = _applicationServiceBase.GetCurrentUserId();
            Guid UserId = new Guid();
            if (Id != null)
            {
                try
                {
                    UserId = new Guid(Id);
                }
                catch
                {
                    return 0;
                }
            }
            var UserLike = await _context.Users.FindAsync(UserId);
            var Playlist = await _context.PlayLists.FindAsync(IdPlaylist);
            if (Playlist == null)
                return -404;
            var rs = await _context.User_Like_Playlists.Where(x => x.UserId == UserId && x.PlaylistId == IdPlaylist).ToListAsync();
            if (rs.Count == 0)
                return -1;//Chua duoc like-- tuong tu da unlike
            _context.User_Like_Playlists.Remove(rs.First());
            Playlist.TotalLike--;
            _context.PlayLists.Update(Playlist);
            return await _context.SaveChangesAsync();
        }
        #endregion

        #region Comment
        public async Task<int> CmtPlaylist( UserCmtRequest rq)
        {
            if (!_applicationServiceBase.CheckAuthentication())
                return -401;
            var Id = _applicationServiceBase.GetCurrentUserId();
            Guid UserId = new Guid();
            if (Id != null)
            {
                try
                {
                    UserId = new Guid(Id);
                }
                catch
                {
                    return 0;
                }
            }
            var UserCmt = await _context.Users.FindAsync(UserId);
            var Playlist = await _context.PlayLists.FindAsync(rq.ObjId);
            if (Playlist == null)
                return -404;
            var ucp = new User_Cmt_Playlist()
            {
                UserId = UserId,
                User = UserCmt,
                PlaylistId = rq.ObjId,
                PlayList = Playlist,
                DateCreate = DateTime.Now,
                Content = rq.Content


            };
            //var rs = await _context.User_Cmt_Songs.FindAsync(rq.UserId, rq.ObjId);
            //if (rs != null)
            //    return -1;//Bi trung -- bai hat da duoc cmt
            _context.User_Cmt_Playlists.Add(ucp);
            Playlist.TotalCmt++;
            _context.PlayLists.Update(Playlist);
            return await _context.SaveChangesAsync();
        }

        public async Task<List<PlayListModel>> GetPlaylistByListId(List<int> ListId)
        {
            var data = await _context.PlayLists
                .Where(x => ListId.Contains(x.Id)).ToListAsync();
            var rs = data.Select(x => MapPlaylist(x)).ToList();
            return rs;
        }
        #endregion
    }
}

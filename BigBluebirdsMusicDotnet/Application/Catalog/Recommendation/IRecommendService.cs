﻿using Models.Catalog.PlayList;
using Models.Catalog.Song;
using Models.Page;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Catalog.Recommendation
{
    public interface IRecommendService
    {
        //Task<List<SongModel>> GetRecommendSongBySinger(PagingRequest pg);
        //Task<List<SongModel>> GetRecommendSongByType(PagingRequest pg);
        //Task<List<SongModel>> GetRecommendSongByTag(PagingRequest pg);
        //Task<List<SongModel>> GetRecommendSongByFriend(PagingRequest pg);


        Task<List<string>> GetListRawSimilarUsersID(Guid userID, int NumberUser, string ActionType, string ObjectType);
        Task<List<int>> GetListLikeObjectsOfAUser(Guid userID, int NumberSong, string ActionType, string ObjectType);
        Task<List<int>> GetListRawSimilarObjectsID(int objid, string ActionType, string ObjectType);
        Task<List<string>> GetListUsersLikeObject(int objId, string ActionType, string ObjectType);

        Task<List<SongModel>> UserBasedCollaborativeFilterSong(Guid userID, int NumberUser, int NumberSong, string ActionType, string ObjectType, PagingRequest pg);
        Task<List<PlayListModel>> UserBasedCollaborativeFilterPlaylist(Guid userID, int NumberUser, int NumberSong, string ActionType, string ObjectType, PagingRequest pg);

        Task<List<SongModel>> ItemBasedCollaborativeFilterSong(int objId, string ActionType, string ObjectType,PagingRequest pg);
        Task<List<PlayListModel>> ItemBasedCollaborativeFilterPlaylist(int objId, string ActionType, string ObjectType, PagingRequest pg);

    }
}

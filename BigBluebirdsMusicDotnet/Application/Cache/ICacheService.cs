﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Cache
{
    public interface ICacheService
    {
        Task<int> AddCache(CacheData rq);
        Task<string> GetCache(string OwnerId,string Type);
        Task<int> DeleteCahe();
    }
}

﻿using Models.System.FacebookAuth;
using Models.System.User;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Application.System.FacebookAuth
{
    public class FacebookAuthService : IFacebookAuthService
    {
        private const string TokenValidationUrl = "https://graph.facebook.com/debug_token?input_token={0}&access_token={1}|{2}";
        private const string UserInforUrl = "https://graph.facebook.com/me?fields=first_name,last_name,picture,email&access_token={0}";
        private readonly IHttpClientFactory _httpClientFactoty;
        private const string AppId = "1007359859770404";
        private const string AppSecret = "1ec1e1c31c5998642172c70c3026c499";
        public FacebookAuthService(IHttpClientFactory httpClientFactoty)
        {
            _httpClientFactoty = httpClientFactoty;
        }

        public async Task<FBValidateTokenModel> ValidateAccessToken (string Token)
        {
            var FormatUrl = string.Format(TokenValidationUrl, Token, AppId, AppSecret);
            var rs = await _httpClientFactoty.CreateClient().GetAsync(FormatUrl);
            rs.EnsureSuccessStatusCode();
            var res = await rs.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<FBValidateTokenModel>(res);
        }

        public async Task<FBUserInforModel> GetUserInfor(string Token)
        {
            var FormatUrl = string.Format(UserInforUrl,Token);
            var rs = await _httpClientFactoty.CreateClient().GetAsync(FormatUrl);
            rs.EnsureSuccessStatusCode();
            var res = await rs.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<FBUserInforModel>(res);
        }

    }
}
